<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    //
    public function subsubcategories()
    {
        return $this->hasMany(Subsubcategory::class);
    }
    public function maincategory()
    {
        return $this->belongsTo(Maincategory::class);
    }
}
