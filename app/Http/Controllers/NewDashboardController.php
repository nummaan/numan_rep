<?php

namespace App\Http\Controllers;

use App\Chatroom;
use App\Post;
use App\User;
use Auth;
use Illuminate\Http\Request;
class NewDashboardController extends Controller
{
    //
    public function index()
    {
        $id = Auth::user()->id;

        $receiver = $this->receivers($id);
        //aa($receiver);
        $user = User::find($id);

        $helpDeskUser = 0;

        $posts = Post::get();
       
        return view('layouts.newapp')->with('receivers', $receiver)->with('requestmaker', $id)->with('helpDeskUser', $helpDeskUser)->with('roomId', 0)->with('postsss',$posts);
    }
    public function receivers($id)
    {

        $receiver = array();
        $is_admin = Auth::user()->name;

        $chatroom = Chatroom::where('chatRoomId', 'Like', '%' . $id . '%')->orderBy('updated_at')
            ->get();
        foreach ($chatroom as $chat)
        {
            $arr = explode(',', $chat->chatRoomId);
            if ($arr[0] == $id)
            {
                array_push($receiver, User::find($arr[1]));
            }
            elseif ($arr[1] == $id)
            {
                array_push($receiver, User::find($arr[0]));
            }
            else
            {
                continue;
            }

        }
        return $receiver;
    }
}
