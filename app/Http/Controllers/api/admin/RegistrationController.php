<?php

namespace App\Http\Controllers\api\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\verifymail;
use App\User;
use App;

class RegistrationController extends Controller
{
    public function register(Request $request) {
        $base_url = App::make('url')->to('/');
    
        $user=new User;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->status=1;
        $user->verify_tokekn=Str::random(40);
        $user->verify_status=5;
        $user->referal_code = generateReferalCode();
        $user->save();
        $thisuser=User::find($user->id);
        $url = $base_url."/api/verified_email/".$user->email.'/'.$user->verify_tokekn;
        
        if(isset($thisuser)) {
            $headers = "From:sumit.innovious@gmail.com\r\n";
            $headers .= "Reply-To: sumit.innovious@gmail.com\r\n";
            $headers .= "Return-Path: sumit.innovious@gmail.com\r\n";
            $to = $request->email;
            $subject = "Verification Mail";
            $message = "Click on this link to verify ".$url;
            if(mail($to,$subject,$message,$headers) != FALSE) {
                $status = '200';
                $msg = 'Success';
                return getJsonResponse($status,$msg);
            } else {
                $status = '400';
                $msg = 'Something Went Wrong!';
                return getJsonResponse($status,$msg);
            }
        }

    }

    public function sendverifyemail($thisuser){
        Mail::to($thisuser['email'])->send(new verifymail($thisuser));
    }
    
    public function verified_email($email,$verify_tokekn){
        $user=User::where(['email'=>$email,'verify_tokekn'=>$verify_tokekn])->first();
        if($user){
            User::where(['email'=>$email,'verify_tokekn'=>$verify_tokekn])->update(['verify_status'=>1,'verify_tokekn'=>NULL]);
            $status = '200';
            $msg = 'Success';
            return getJsonResponse($status,$msg);
        }else{
            $status = '400';
            $msg = 'Email not verified.Try again';
            return getJsonResponse($status,$msg);
        }
    }
}