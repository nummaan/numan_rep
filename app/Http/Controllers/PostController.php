<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Post;
use App\State;
use App\PostCategory;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category_id)
    {

        $category = PostCategory::find($category_id);
        $parentCategory = null;
        if(!empty($category->category) && $category->category != null){
            $parentCategory = $category->category;
        }else{
            $parentCategory = $category->main_category;
        }

        $bcategories = [
            $category->post,
            $parentCategory
        ];
        
        $countries = Country::get();
        $states    = State::where('country_id',1)->get();
        $cities    = City::where('state_id',1)->get();

        return view('posts.create',compact('category_id','countries','states','cities','bcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "category"       => "required",
            "title"          => "required",            
            "price"          => "required",
            "expired_at"     => "required",
            "reffral"        => "required",
            "country"        => "required",
            "state"          => "required",
            "city"           => "required",
            "address"        => "required",
            "featured_image" => "required|image"
        ]);

        //product image upload
        $path    = $request->file('featured_image')->store('posts/images',['disk' => 'public']);
        // $pathArr = explode('/', $path);
        // unset($pathArr[0]);
        // $path    = implode('/', $pathArr); 

        \DB::beginTransaction();
        try {
            
            $post =  Post::create([
                "category"   => $request->category,
                "title"      => $request->title,
                "content"    => $request->content,      
                "price"      => $request->price,
                "expired_at" => $request->expired_at,
                "reffral"    => $request->reffral,
                "country"    => $request->country,
                "state"      => $request->state,
                "city"       => $request->city,
                "address"    => $request->address,
                "image"      => $path
            ]);

            \DB::commit();
            return redirect()->route('newdashboard')->with('success','Post '. $post->title.' saved!');


        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->route('newdashboard')->with('error',$e->getMessage()); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
