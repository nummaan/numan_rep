<?php

namespace App\Http\Controllers;

use App\PostCategory;
use Illuminate\Http\Request;
use Session;

class PostCategoryController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        //
            echo $request->deleteId;
            if(!empty($request->deleteId))
            {
                $postCategory = PostCategory::find($request->deleteId);
                $postCategory->delete();
                Session::flash('success', 'Post Category Created Deleted');
                return redirect()->back();
            }
            else
            {
                $postCat = PostCategory::find($request->catId);
                $postCat->post = $request->input('postName');
                $postCat->main_category = $request->input('category');
                $postCat->category = $request->input('sub_category');
                $postCat->save();

                Session::flash('success', 'Post Category Updated Successfully');
                return redirect()->back();
            }
            
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            // Validating Request
            $this->validate($request, [
                "category"      => "required|string|in:Sell,Buy,Blog,Event",
                "sub_category"  => "required|string|max:191",
                "postName"      => "required|string|max:191"
            ]);

            // Checking For Duplicate
            $duplicate = PostCategory::where('post', $request->postName)
                                        ->where('main_category', $request->category)
                                        ->where('category', $request->sub_category)
                                        ->first();

            if($duplicate != null) {
                return back()->withErrors(['postName' => "You Can't Add A Same Category"]);
            }

            $postCat = new PostCategory();
            $postCat->post = $request->postName;
            $postCat->main_category = $request->category;
            $postCat->category = $request->sub_category;
            $postCat->save();

            // WE ARE NOT SHOWING ANY ERROR OR SUCCESS MESSAGE
            // Session::flash('success', 'Post Category Created Successfully');
            return back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PostCategory $postCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostCategory $postCat)
    {
            $postCat->post = $request->post;
            $postCat->category = $request->category;
            $postCat->save();

            Session::flash('success', 'Post Category Updated Successfully');
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postCategory = PostCategory::findOrFail($id);
        $postCategory->delete();
        
        return response()->json(['message' => "success"], 200);

    }
}
