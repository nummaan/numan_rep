<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Event;
use App\ReferralPost;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $userInsert = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'status' => 1,
            'verify_status' => 5,
            'password' => Hash::make($data['password']),
            'referal_code' => generateReferalCode()
        ]);
        if($userInsert)
        {
            if(!empty($data['fbId']))
            {
                $userId = $userInsert->id;
                ReferralPost::create([
                    'user_id'=> $userId,
                    'referred_id'=> $data['user_id'],
                    'event_id'=> $data['event_id'],
                    'post_type'=> 'event',
                ]);
            } 
            
        }
        return $userInsert;
    }

    public function verifyUser($id)
    {
        $verifyUser = User::where('id', $id)->first();
        if(isset($verifyUser) ){
            //$user = $verifyUser->verify_status;
            if($verifyUser->verify_status != 1) {
                $verifyUser->verify_status = 1;
                $verifyUser->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            Session::flash('warning', 'Sorry your email cannot be identified.');
            return redirect('/login');
        }
        Session::flash('success', $status);
        return redirect('/login');
    }
}
