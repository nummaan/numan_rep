<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maincategory extends Model
{
    //
    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }
}
