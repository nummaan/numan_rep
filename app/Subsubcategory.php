<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsubcategory extends Model
{
    //
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }
}
