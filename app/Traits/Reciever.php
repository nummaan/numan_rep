<?php
    
namespace App\Traits;
use Auth;
use App\Chatroom;
use App\User;

trait Reciever
{
    public function receivers($id)
    {

        $receiver = array();
        $is_admin = Auth::user()->name;

        $chatroom = Chatroom::where('chatRoomId', 'Like', '%' . $id . '%')->orderBy('updated_at')
            ->get();
        foreach ($chatroom as $chat)
        {
            $arr = explode(',', $chat->chatRoomId);
            if ($arr[0] == $id)
            {
                array_push($receiver, User::find($arr[1]));
            }
            elseif ($arr[1] == $id)
            {
                array_push($receiver, User::find($arr[0]));
            }
            else
            {
                continue;
            }

        }
        return $receiver;
    }
}