<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MaincategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('maincategories')->insert([
            'name' => 'Buy',
        ]);
        DB::table('maincategories')->insert([
            'name' => 'Sell',
        ]);
        DB::table('maincategories')->insert([
            'name' => 'Blog',
        ]);
        DB::table('maincategories')->insert([
            'name' => 'Event',
        ]);
    }
}
