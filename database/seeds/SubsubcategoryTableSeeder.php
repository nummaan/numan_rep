<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SubsubcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('subsubcategories')->insert([
            'name' => 'A-Service',
            'subcategory_id' => '1'
        ]);
        DB::table('subsubcategories')->insert([
            'name' => 'B-Service',
            'subcategory_id' => '1'
        ]);
        DB::table('subsubcategories')->insert([
            'name' => 'A-Product',
            'subcategory_id' => '2'
        ]);
        DB::table('subsubcategories')->insert([
            'name' => 'B-Product',
            'subcategory_id' => '2'
        ]);
    }
}
