<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SubcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('subcategories')->insert([
            'name' => 'Service',
            'maincategory_id' => '1'
        ]);
        DB::table('subcategories')->insert([
            'name' => 'Product',
            'maincategory_id' => '1'
        ]);
        DB::table('subcategories')->insert([
            'name' => 'Service',
            'maincategory_id' => '2'
        ]);
        DB::table('subcategories')->insert([
            'name' => 'Product',
            'maincategory_id' => '2'
        ]);
        DB::table('subcategories')->insert([
            'name' => 'News Blog',
            'maincategory_id' => '3'
        ]);
        DB::table('subcategories')->insert([
            'name' => 'Music Event',
            'maincategory_id' => '4'
        ]);
    }
}
