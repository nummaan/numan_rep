<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title",255);
            $table->longText("content");            
            $table->string("image",255);
            $table->integer("price");
            $table->integer("reffral");
            $table->integer("category");
            $table->integer("country");
            $table->integer("state");
            $table->integer("city");            
            $table->text("address");
            $table->integer("created_by");
            $table->timestamp("expired_at");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
