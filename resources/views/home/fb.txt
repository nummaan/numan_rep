
==== Query
  curl -i -X POST \
   "https://graph.facebook.com/v6.0/http://splashthemepark.com/home?access_token=<access token sanitized>"
==== Access Token Info
  {
    "perms": [
      "public_profile"
    ],
    "user_id": 2596448037268784,
    "app_id": 558038808129662
  }
==== Parameters
- Query Parameters


  {}
- POST Parameters


  {}
==== Response
  {
    "url": "http://splashthemepark.com/login",
    "type": "website",
    "title": "Favicon",
    "image": [
      {
        "url": "http://splashthemepark.com/images/loading.gif"
      },
      {
        "url": "http://splashthemepark.com/images/eventLogo.png"
      },
      {
        "url": "http://splashthemepark.com/images/Taxic.png"
      },
      {
        "url": "http://splashthemepark.com/images/photo.png"
      },
      {
        "url": "http://splashthemepark.com/images/Taxi.png"
      },
      {
        "url": "http://splashthemepark.com/images/Taxica.png"
      },
      {
        "url": "http://splashthemepark.com/images/image_not_found.jpg"
      },
      {
        "url": "http://splashthemepark.com/images/going.png"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsImages/1579237736.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsImages/1579237765.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsImages/1579237898.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsImages/1579237973.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/avatars/1579236615.png"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238059.png"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238103.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238154.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238259.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238299.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579239097.jpeg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579239131.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579239163.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579239203.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579239242.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579239278.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238824.png"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238855.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238889.jpeg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238923.png"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579238953.jpg"
      },
      {
        "url": "http://splashthemepark.com/uploads/adsimages/1579239006.jpg"
      }
    ],
    "description": "Uber Technologies,inc.(\"UTI\"), the company that contracts with drivers who provide peer-to-peer transportation services througe the Uber mobile app in California, in committed to safety. As part of that commitment,UTI may obtain \"consumer report\" and/or \"investigative consumer reports\" (background c…",
    "updated_time": "2020-02-17T17:17:55+0000",
    "id": "3334702643222866"
  }
==== Debug Information from Graph API Explorer
- https://developers.facebook.com/tools/explorer/?method=POST&path=http%3A%2F%2Fsplashthemepark.com%2Fhome&version=v6.0