@extends('layouts.post')

@section('content')

<section class="create-post">
	<form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
		@csrf
		<div class="container">		
			<div class="row">
				<div class="bredcums">
					<ul class="category-hirarchy">
						@if(count($bcategories) > 0)
							@foreach($bcategories as $key => $cate)
								<li class="category-hirarchy-item">
									<a href="">{{ $cate }}</a>&nbsp;&nbsp;
									<i class="fas fa-angle-right"></i>
								</li>
							@endforeach
						@endif
						<input type="hidden" name="category" value="{{ $category_id}}">
					</ul>

					<ul class="button">
						<li class="button-item"><button type="submit" class="btn btn-default btn-submit"><i class="fas fa-check"></i>&nbsp;SUBMIT </button></li>
						<li class="button-item"><a href="#" class="btn btn-default btn-cancel"><i class="fas fa-times-circle"></i>&nbsp;CANCEL </a></li>
					</ul>
				</div>
			</div>
	
			<div class="row">			
				<div class="col-md-7">
					<div class="form post-form">					
						<div class="form-group">				
							<input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Green Eyed Cates">
							@if ($errors->has('title'))
                                <div class="alert alert-danger">{{ $errors->first('title') }}</div>
                            @endif
						</div>
						<div class="form-group">
							<div class="image-upload">
								<label for="file-input">
									<img id="image-preview" src="{{ asset('images/defaults/dummy_img.png') }}" style="pointer-events: none"/>
								</label>
								<input id="file-input" type="file" name="featured_image" data-preview="image-preview" />
							</div>
							@if ($errors->has('featured_image'))
                                <div class="alert alert-danger">{{ $errors->first('featured_image') }}</div>
                            @endif
						</div>
						<div class="form-group">
							<textarea class="summernote" name="content">{{ old('content') }}</textarea>
						</div>					
					</div>
				</div>
				<div class="col-md-5">
					<div class="price-box">
						<div class="price-btn-container">
							<button class="btn btn-default price-btn price-bages" >
								Buy $0
							</button>
							<hr>
						</div>
						<div class="price-details">
							<table>
								<tr>
									<td>
										<label for=""><i class="far fa-money-bill-alt"></i>&nbsp;&nbsp;<b>Price</b></label>
									</td>
									<td>
										<input type="text" name="price" id="price" value="{{ old('price') }}">
									</td>
								</tr>
								<tr>
									<td colspan="2">
										@if ($errors->has('price'))
                                            <div class="alert alert-danger">{{ $errors->first('price') }}</div>
                                        @endif
                                    </td>
								</tr>
								<tr>
									<td>
										<label for=""><i class="far fa-calendar-check"></i>&nbsp;&nbsp;<b>Posted at</b></label>
									</td>
									<td>
										<label for="">{{ date('d M Y') }}</label>
									</td>
								</tr>
								<tr>
									<td>
										<label for=""><i class="far fa-calendar-check"></i><b>&nbsp;&nbsp;Expired at</b></label>
									</td>
									<td>
										<input type="text" name="expired_at" id="expired_at" class="" value="{{ old('expired_at') }}"> hour
									</td>
								</tr>
								<tr>
									<td colspan="2">
										@if ($errors->has('expired_at'))
                                            <div class="alert alert-danger">{{ $errors->first('expired_at') }}</div>
                                        @endif
                                    </td>
								</tr>
							</table>							
						</div>
					</div>

					<div class="price-box">
						<div class="price-btn-container">
							<h3>Genral Info</h3>
							<hr>
						</div>
						<div class="price-details">
							<table>
								<tr>
									<td>
										<input type="text" name="reffral" id="reffral" class="" value="{{ old('reffral') }}">
									</td>
									<td>
										<label for=""><i class="fas fa-globe"></i>&nbsp;&nbsp;<b>% Referral</b></label>										
									</td>									
								</tr>
								<tr>
									<td colspan="2">
										@if ($errors->has('reffral'))
                                            <div class="alert alert-danger">{{ $errors->first('reffral') }}</div>
                                        @endif
									</td>
								</tr>
								<tr>
									<td>
										<label for=""><i class="fas fa-globe"></i>&nbsp;&nbsp;<b>Country</b></label>
									</td>
									<td>
										<select name="country" id="state">
											{{-- @if(count($countries) > 0)
											@foreach($countries as $key => $country)
												<option value="{{ $country->id }}">{{ $country->name }}</option>
											@endforeach
											@endif --}}
											<option value="1">USA</option>
											<option value="2">Canda</option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										@if ($errors->has('country'))
                                            <div class="alert alert-danger">{{ $errors->first('country') }}</div>
                                        @endif
                                    </td>
								</tr>
								<tr>
									<td>
										<label for=""><i class="far fa-flag"></i>&nbsp;&nbsp;<b>State</b></label>
									</td>
									<td>
										<select name="state" id="state">
											{{--@if(count($states) > 0)
											@foreach($states as $key => $state)
												<option value="{{ $state->id }}">{{ $state->name }}</option>
											@endforeach
											@endif --}}
											<option value="1">Alabama</option>
											<option value="2">Alaska</option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										@if ($errors->has('state'))
                                            <div class="alert alert-danger">{{ $errors->first('state') }}</div>
                                        @endif
                                    </td>
								</tr>
								<tr>
									<td>
										<label for=""><i class="fas fa-chart-area"></i><b>&nbsp;&nbsp;City</b></label>
									</td>
									<td>
										<select name="city" id="city">
											{{-- @if(count($cities) > 0)
											@foreach($cities as $key => $city)
												<option value="{{ $city->id }}">{{ $city->name }}</option>
											@endforeach
											@endif --}}
											<option value="1">Montgomery</option>
										</select>
									</td>
								</tr>
								<tr>
									<td>
										<label for=""><i class="fas fa-map-marker-alt"></i><b>&nbsp;&nbsp;Address</b></label>
									</td>
									<td>
										<input type="text" name="address" id="address" value="{{ old('address') }}">
									</td>
								</tr>
								<tr>
									<td colspan="2">
										@if ($errors->has('address'))
                                            <div class="alert alert-danger">{{ $errors->first('address') }}</div>
                                        @endif
                                    </td>
								</tr>
							</table>							
						</div>
					</div>
				</div>			
			</div>
		</div>
	</form> 
</section>
@endsection

@push('footerjs')
<script type="text/javascript" src="{{ asset('js/summernote-bs4.min.js') }}"></script>
<script>

	$(document).ready(function(){
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            //$(this).next('.item_image_label').html(fileName);
            readURL(this);              
        });

        $('.summernote').summernote({
	        height: 200,
	        tabsize: 2
	    });
	    $("#price").on('change keyup keydown',function(){
	    	if($(this).val().length == 0){
	    		$('.price-bages').html("Buy $0");
	    	}else{
	    		$('.price-bages').html("Buy $"+$(this).val());
	    	}
	    	
	    });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+$(input).data('preview')).attr('src', e.target.result);   
                //$('#'+$(input).data('preview')).next('span').show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endpush