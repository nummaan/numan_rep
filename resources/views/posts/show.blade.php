@extends('layouts.post')

@section('content')

<section class="create-post">		
	<div class="container">	
		<div class="row">
			<div class="bredcums">
				<ul class="category-hirarchy">
					<li class="category-hirarchy-item"><a href="">Category 1 </a><i class="fas fa-angle-right"></i></li>
					<li class="category-hirarchy-item"><a href="">Category 2 </a><i class="fas fa-angle-right"></i></li>
					<li class="category-hirarchy-item"><a href="">Category 3 </a><i class="fas fa-angle-right"></i></li>
					<input type="hidden" name="category" value="{{ $post->category}}">
				</ul>

				<ul class="button">
					<li class="button-item"><button type="submit" class="btn btn-default btn-submit"><i class="fas fa-check"></i>&nbsp;SUBMIT </button></li>
					<li class="button-item"><a href="#" class="btn btn-default btn-cancel"><i class="fas fa-times-circle"></i>&nbsp;CANCEL </a></li>
				</ul>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-8 ">
				<h2 class="font-weight-bold" style="font-family: 'Poppins', sans-serif !important;text-align: left;">{{ $post->title }}</h2>
				<div class="card shadow rounded mb-3 set">
					<div class="position-relative">
						<a href="{{ route('posts.show',["post" => $post->id]) }}" >
							<img class="card-img-top"  src="{{ Storage::url($post->image)  }}" style="height: 200px" alt="Card image cap">
						</a>
						
						<span class="overlay_badge_buy savedButton" id="savedButton" style="background: none;padding: 0;">
							<a href="#" onclick="saveBuySell('{{$post->id}}','{{auth()->id()}}','buy')">
								<i id="savedBuy{{$post->id}}" class="fas fa-heart text-danger bg-white p-1"></i>                                
							</a>
						</span>
						<div class="text-right mb-2 position-absolute" style="right:0; bottom: -6px;">
							<span data-time="{{$post->created_at->addHours($post->hour)}}" class="bg-danger text-white p-1 countDownTimer" id="showCountDownTimer">
								{{$post->hour}}
							</span>
						</div>
					</div>
					
					<div class="card-body p-1">
						
						<p class="card-text"style="font-size: 14px; !important;font-family: 'Poppins', sans-serif !important; overflow: hidden; line-height: 1.2em;">
							{{ strip_tags($post->content) }}
						</p>

						<div class="row" style="font-size: 0.72rem !important;">
							<div class="col-3 pl-4 pr-1">
								<span>
									<i class="far fa-comment-alt"></i> <sup>6</sup>
								</span>
								<span>
									<i class="far fa-thumbs-up"></i> <sup>8</sup>
								</span>
							</div>
						</div>
						<hr class="bg-secondary my-1" style="background-color: #6c757d!important;height:1px;">
						<div class="text-right mb-3" style="font-size: 0.72rem !important;">
							{{ $post->buyer_location }}
						</div>
						<div class="row mb-2">							
							<div class="col-9">
								<h6 class="font-weight-bold">{{ isset($user) ? $user->name : "" }}</h6>
								<div>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
			<div class="col-md-4">
				
			</div>
		</div>			
	</div>	
</section>
@endsection