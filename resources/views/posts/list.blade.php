@php 
    $buyers = \App\Buyer::all();

    $checkPermission = DB::table('user_menu')->where('menu_options_id', '=', '18')->where('user_id', '=', auth()->id())->get()->first();
    if ($checkPermission) {
        $permission = 1;
    } else {
        $permission = 0;
    }
@endphp
<div class="row">
    @isset($buyers)
        @foreach($buyers as $buyer)

            @php
                $bidAmount = bidAmountIfAlreadyBid($buyer,'buy');
                $isPostSaved = isSavedPost($buyer->id,'buy',auth()->id());
                $bid = $buyer->bids()->where('user_id',auth()->id())->first();

            @endphp
            <div class="col-md-12 hoverSet">
                <div class="card shadow rounded mb-3 set">
                    <div class="row">
                        <div class="col-4 py-2 px-4">
                            <div class="position-relative">
                                <a href="#" data-bids="{{getPostTotalBids($buyer,'buy')}}"
                                    data-orders="{{getPostBidOrders($buyer,'buy')}}" data-isSaved="{{$isPostSaved}}"
                                    data-user-name="{{$buyer->user->name}}" data-avatar="{{$buyer->user->avatar}}"
                                    data-all="{{json_encode($buyer)}}"
                                    onclick="showPostDetails('{{$buyer->id}}','{{auth()->id()}}',this)">
                                    <img class="card-img-top"
                                        src="{{$buyer->buyer_featured_image?"uploads/buyer/".$buyer->buyer_featured_image:"/images/image_not_found.jpg"}}"
                                        style="height: 200px" alt="Card image cap">
                                </a>
                                <span class="overlay_badge_buy">Buy ${{getCurrentRate($buyer)}}</span>
                                @if(auth()->id()==$buyer->user_id || $permission==1)
                                <span class="overlay_badge_buy buyDelete" id="buyDelete" style="background: none;padding: 0;">
                                    <a href="#" onclick="deleteBuy('{{$buyer->id}}')">
                                        <i class="fas fa-trash-alt text-dark bg-white p-1"></i>
                                    </a>
                                </span>
                                <span class="overlay_badge_buy buyEdit" id="buyEdit" style="background: none;padding: 0;">
                                    <a href="#" onclick="buyEdit('{{$buyer->id}}','{{$buyer->user_id}}')" class="">
                                        <i class="fas fa-edit text-primary bg-white p-1"></i>
                                    </a>
                                </span>
                                @endif
                                <span class="overlay_badge_buy savedButton" id="savedButton"
                                    style="background: none;padding: 0;">
                                    <a href="#" onclick="saveBuySell('{{$buyer->id}}','{{auth()->id()}}','buy')">
                                        <i id="savedBuy{{$buyer->id}}" class="fas fa-heart text-danger bg-white p-1"></i>
                                        
                                    </a>
                                </span>
                                <div class="text-right mb-2 position-absolute" style="right:0; bottom: -6px;">
                                    <span data-time="{{$buyer->created_at->addHours($buyer->hour)}}"
                                        class="bg-danger text-white p-1 countDownTimer" id="showCountDownTimer">
                                        {{$buyer->hour}}</span>
                                </div>
                            </div>
                        </div> 
                        <div class="col-8 py-2 px-4">
                            <div class="row mb-2 position-relative" style="font-size: 0.72rem !important;">
                
                                <div class="col-3 pl-4 pr-1">
                                    <span>
                                        <i class="far fa-comment-alt"></i> <sup>6</sup>
                                    </span>
                                    <span>
                                        <i class="far fa-thumbs-up"></i> <sup>8</sup>
                                    </span>
                                </div>
                                <div class="col-5 px-md-0 px-4">
                                    <span>
                                        @if($buyer->user_id==auth()->id())
                                        <a class="text-secondary" onclick="buySellBids('{{$buyer->id}}','{{getPostTotalBids($buyer,'buy')}}')"
                                            href="#">
                                            {{getPostTotalBids($buyer)}} bid </a>
                                        @else
                                        {{getPostTotalBids($buyer)}} bid 
                                        @endif
                                    </span>
                                    <span>
                                        @if($buyer->user_id==auth()->id())
                                        <a class="text-secondary" onclick="buySellOrder('{{$buyer->id}}','{{getPostBidOrders($buyer,'buy')}}')"
                                            href="#">
                                            {{getPostBidOrders($buyer)}} orders</a>
                                        @else
                                        {{getPostBidOrders($buyer)}} orders
                                        @endif
                                    </span>
                                </div>
                                <div class="col-4">
                                    @if((isset($bid) && $bid->status =='pending') || !isset($bid))
                                    <input type="text" value="{{$bidAmount}}" class="bidinput position-absolute" data-id="{{$buyer->id}}" size="4"
                                        style="align-self: center; width: 60px; right:74px;">
                                    <button data-id="{{$buyer->id}}" data-max="{{getCurrentRate($buyer)}}"
                                        class="btn btn-primary btn-sm py-0 px-1 position-absolute triggerBid"
                                        style="font-size: 0.72rem !important; right:15px;">
                                        Place Bid
                                    </button>
                                    
                                    @endif
                        
                                </div>
                            </div>
                            <div class="card-body p-1">
                                <h6 class="card-title font-weight-bold" style="font-size: 1.125rem !important;
                                font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 1.3em; line-height: 1.2em;">{{ $buyer->buyer_pro_title }}</h6>
                                
                                <p class="card-text"style="font-size: 14px; !important;
                                font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 2.4em; line-height: 1.2em;">
                                    {{ $buyer->buyer_pro_description }}
                                </p>
                        
                                <div class="row" style="font-size: 0.72rem !important;">
                                    <div class="col-6">
                                        {{ date_format($buyer->created_at,'d F Y')  }}
                                    </div>
                                    <div class="col-6 text-right">
                                        {{  date_diff($buyer->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago')  }}
                        
                                    </div>
                                </div>
                                <hr class="bg-secondary my-1" style="background-color: #6c757d!important;height:1px;">
                                <div class="text-right mb-3" style="font-size: 0.72rem !important;">
                                    {{ $buyer->buyer_location }}
                                </div>
                                <div class="row mb-2">
                                    <div class="col-3 pr-1">
                                        @if (auth()->id() == $buyer->user_id)
                                        <img src="{{ asset('uploads/avatars/'.App\User::find($buyer->user_id)->avatar) }}" class="rounded-circle" style="width:50px; height:50px;" alt="person">
                                            
                                        @else
                                        <img src="{{ asset('uploads/avatars/'.App\User::find($buyer->user_id)->avatar) }}" style="width:50px; height:50px;" alt="person">
                                            
                                        @endif
                                    </div>
                                    <div class="col-9">
                                        <h6 class="font-weight-bold">{{ App\User::find($buyer->user_id)->name }}</h6>
                                        <div>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="font-size: 0.72rem !important;">
                                    <div class="col-md-6 text-right">
                                        {{$buyer->buyer_commission_percentage}}% Referal
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fb-share-button" data-href="http://splashthemepark.com/home" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsplashthemepark.com%2Fhome&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
                <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
                    <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                        {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                        <img class=""
                            src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                            alt="Card image cap">
                        <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                    </a>
                    <?php 
                $addCnt++; 
                ?>
                </div>
                <?php } ?>
        @endforeach
    @endisset
</div>