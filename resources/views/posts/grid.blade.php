{{-- @php 
    $buyers = \App\Buyer::all();

    $checkPermission = DB::table('user_menu')->where('menu_options_id', '=', '18')->where('user_id', '=', auth()->id())->get()->first();
    if ($checkPermission) {
        $permission = 1;
    } else {
        $permission = 0;
    }
@endphp --}}


<div class="row"> 
    @foreach($postsss as $post)      
        @php 
            $user = \App\User::find($post->created_by);
        @endphp     
        <div class="col-md-4 hoverSet">
            <div class="card shadow rounded mb-3 set">
                <div class="position-relative">
                    <a href="{{ route('posts.show',["post" => $post->id]) }}" >
                        <img class="card-img-top"  src="{{ Storage::url($post->image)  }}" style="height: 200px" alt="Card image cap">
                    </a>
                    <span class="overlay_badge_buy">Buy ${{ $post->price }}</span>
                    @if(auth()->id() == $post->created_by)
                    <span class="overlay_badge_buy buyDelete" id="buyDelete" style="background: none;padding: 0;">
                        <a href="#" onclick="deleteBuy('{{$post->id}}')">
                            <i class="fas fa-trash-alt text-dark bg-white p-1"></i>
                        </a>
                    </span>
                    <span class="overlay_badge_buy buyEdit" id="buyEdit" style="background: none;padding: 0;">
                        <a href="#" onclick="buyEdit('{{$post->id}}','{{$post->created_by}}')" class="">
                            <i class="fas fa-edit text-primary bg-white p-1"></i>
                        </a>
                    </span>
                    @endif
                    <span class="overlay_badge_buy savedButton" id="savedButton" style="background: none;padding: 0;">
                        <a href="#" onclick="saveBuySell('{{$post->id}}','{{auth()->id()}}','buy')">
                            <i id="savedBuy{{$post->id}}" class="fas fa-heart text-danger bg-white p-1"></i>                                
                        </a>
                    </span>
                    <div class="text-right mb-2 position-absolute" style="right:0; bottom: -6px;">
                        <span data-time="{{$post->created_at->addHours($post->hour)}}" class="bg-danger text-white p-1 countDownTimer" id="showCountDownTimer">
                            {{$post->hour}}
                        </span>
                    </div>
                </div>
                <div class="row mb-2 position-relative" style="font-size: 0.72rem !important;">                
                    <div class="col-3 pl-4 pr-1">
                        <span>
                            <i class="far fa-comment-alt"></i> <sup>6</sup>
                        </span>
                        <span>
                            <i class="far fa-thumbs-up"></i> <sup>8</sup>
                        </span>
                    </div>
                    <div class="col-5 px-md-0 px-4">
                        <span>
                            @if($post->created_by == auth()->id())
                            <a class="text-secondary" onclick="buySellBids('{{$post->id}}','{{getPostTotalBids($post,'buy')}}')"
                                href="#">
                                {{getPostTotalBids($post)}} bid </a>
                            @else
                            {{getPostTotalBids($post)}} bid 
                            @endif
                        </span>
                        <span>
                            @if($post->created_by == auth()->id())
                            <a class="text-secondary" onclick="buySellOrder('{{$post->id}}','{{getPostBidOrders($post,'buy') }}')" href="#">
                                {{ getPostBidOrders($post) }} orders</a>
                            @else
                            {{getPostBidOrders($post)}} orders
                            @endif
                        </span>
                    </div>
                    <div class="col-4">
                        @if((isset($bid) && $bid->status =='pending') || !isset($bid))
                        <input type="text" value="" class="bidinput position-absolute" data-id="{{ $post->id }}" size="4"
                            style="align-self: center; width: 60px; right:74px;">
                        <button data-id="{{ $post->id }}" data-max="{{ $post->price }}" class="btn btn-primary btn-sm py-0 px-1 position-absolute triggerBid"
                            style="font-size: 0.72rem !important; right:15px;">
                            Place Bid
                        </button>
                        <button data-id="{{$post->id}}" data-post-type='buy' class="closebidinput "
                            style="color: #fff;margin-right: 35px;"><i style="display: none" class="fas fa-close"></i>
                        </button>
                        @endif
            
                    </div>
                </div>
                <div class="card-body p-1">
                    <h6 class="card-title font-weight-bold" style="font-size: 1.125rem !important;font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 1.3em; line-height: 1.2em;">{{ $post->title }}</h6>
                    <p class="card-text"style="font-size: 14px; !important;font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 2.4em; line-height: 1.2em;">
                        {{ strip_tags($post->content) }}
                    </p>
            
                    <div class="row" style="font-size: 0.72rem !important;">
                        <div class="col-6">
                            {{ date_format($post->created_at,'d F Y')  }}
                        </div>
                        <div class="col-6 text-right">
                            {{  date_diff($post->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago')  }}                
                        </div>
                    </div>
                    <hr class="bg-secondary my-1" style="background-color: #6c757d!important;height:1px;">
                    <div class="text-right mb-3" style="font-size: 0.72rem !important;">
                        {{ $post->buyer_location }}
                    </div>
                    <div class="row mb-2">
                        <div class="col-3 pr-1">
                            @if (auth()->id() == $post->created_by)
                            <img src="{{-- asset('uploads/avatars/'.App\User::find($post->created_by)->avatar) --}}" class="rounded-circle w-100 h-auto" alt="person">
                                
                            @else
                            <img src="{{-- asset('uploads/avatars/'.App\User::find($post->created_by)->avatar) --}}" class="w-100 h-auto" alt="person">
                                
                            @endif
                        </div>
                        <div class="col-9">
                            <h6 class="font-weight-bold">{{ isset($user) ? $user->name : "" }}</h6>
                            <div>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-size: 0.72rem !important;">
                        <div class="col-md-6 text-right">
                            {{$post->reffral }}% Referal
                        </div>
                        <div class="col-md-6">
                            <div class="fb-share-button" data-href="http://splashthemepark.com/home" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsplashthemepark.com%2Fhome&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
            <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
                <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                    {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                    <img class=""
                        src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                        alt="Card image cap">
                    <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                </a>
                <?php $addCnt++;  ?>
            </div>
        <?php } ?>
    @endforeach   
</div>