<div id="post" class="border border-white p-2" style="border-width:2px !important;">    
    <div>
        <h5>Categories</h5>
        <hr>
        @php        
            $post_categories = \App\PostCategory::groupBy('main_category')->get();
        @endphp
        <ul>
            @foreach ($post_categories as $post_category)
                <li>{{ $post_category->main_category }}</li>
                <ul>
                    @php
                    $subcategories = \App\PostCategory::where('main_category',$post_category->main_category)->groupBy('category')->get();
                        
                    @endphp
                    @foreach ($subcategories as $subcategory)
                    <li>{{ $subcategory->category }}</li>
                        <ul>
                            @php
                                $subsubcategories = \App\PostCategory::where('category',$subcategory->category)->groupBy('post')->get();
                            @endphp
                            @foreach ($subsubcategories as $subsubcategory)
                                <li class="post-sub-category"><a href="{{ route('posts.create',['category_id' => $subsubcategory->id] ) }}">{{ $subsubcategory->post }}</a></li>
                            @endforeach
                        </ul>
                    @endforeach
                </ul>
            @endforeach
        </ul>        
        <div class="text-center">
            <button class="rounded-pill border border-danger text-danger px-3 py-1" style="border-width:2px !important;">Show More</button>
        </div>
        <hr class="bg-secondary">
        <div>
            <div class="text-center px-3">
                <button class="btn btn-block text-white" style="background-color:#7F22A5 !important;">Apply Filter</button>
            </div>
        </div>
    </div>
</div>