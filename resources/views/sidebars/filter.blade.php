<div id="filter" class="border border-white p-2" style="border-width:2px !important;">
    <div>
        <div class="container-fluid mb-3">
            <div class="text-center">
                <span class="px-2">ALL</span>
                <span><i class="fas fa-heart text-danger px-2"></i></span>
            </div>
        </div>
        {{-- <div class="rounded mb-3 font-weight-bold" style="font-size:10px;">
            <nav class="nav justify-content-center">
                <li class="nav-item px-2 py-1 mx-1 bg-danger">
                    <a class="" href="#">SELL</a>
                </li>
                <li class="nav-item px-2 py-1 mx-1 bg-success">
                    <a class="" href="#">BUY</a>
                </li>
                <li class="nav-item px-2 py-1 mx-1 bg-warning">
                    <a class="" href="#">BLOG</a>
                </li>
                <li class="nav-item px-2 py-1 mx-1" style="background-color:#7F22A5 !important;">
                    <a class="" href="#">EVENT</a>
                </li>
            </nav>
        </div> --}}
    </div>
    <div>
        <h5>Categories</h5>
        <hr>
        @php
        // $maincategories = \App\Maincategory::get();
        $post_categories = \App\PostCategory::groupBy('main_category')->get();
        @endphp
        <ul>
            @foreach ($post_categories as $post_category)
                <li>{{ $post_category->main_category }}</li>
                <ul>
                    @php
                    $subcategories = \App\PostCategory::where('main_category',$post_category->main_category)->groupBy('category')->get();
                        
                    @endphp
                    @foreach ($subcategories as $subcategory)
                    <li>{{ $subcategory->category }}</li>
                        <ul>
                            @php
                                $subsubcategories = \App\PostCategory::where('category',$subcategory->category)->groupBy('post')->get();
                            @endphp
                            @foreach ($subsubcategories as $subsubcategory)
                                <li>{{ $subsubcategory->post }}</li>
                            @endforeach
                        </ul>
                    @endforeach
                </ul>
            @endforeach
        </ul>
        {{-- <ul>
            @foreach ($maincategories as $maincategory)
                <ul>
                    <li>
                        {{$maincategory->name}}
                        <ul>
                            @foreach ($maincategory->subcategories as $subcategory)
                            <li>
                                {{$subcategory->name}}
                                <ul>
                                    @foreach ($subcategory->subsubcategories as $subsubcategory)
                                    <li>
                                        {{$subsubcategory->name}}
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            @endforeach

        </ul> --}}
        <div class="text-center">
            <button class="rounded-pill border border-danger text-danger px-3 py-1" style="border-width:2px !important;">Show More</button>
        </div>
        <hr class="bg-secondary">
        <div>
            <p>Price Range</p>
            <hr class="bg-secondary">
            <div class="row mb-3">
                <div class="col">
                    <p>Min</p>
                    <input class="form-control bg-white" type="text" name="min" placeholder="Min">
                </div>
                <div class="col">
                    <p>Max</p>
                    <input class="form-control bg-white" type="text" name="max" placeholder="Max">
                </div>
            </div>
            <hr class="bg-secondary">
            <div class="row mb-3">
                <div class="col-12">
                    <form action="" method="post" class="form-inline">
                        <span>Posted By :</span>
                        <input type="text" class="form-control d-inline bg-white" placeholder="&#xf002; Search" style="width:11.3rem; font-family: FontAwesome, 'Open Sans', Verdana" />
                    </form>
                </div>
            </div>
            <hr class="bg-secondary">
            <div class="text-center px-3">
                <button class="btn btn-block text-white" style="background-color:#7F22A5 !important;">Apply Filter</button>
            </div>
        </div>
    </div>
</div>