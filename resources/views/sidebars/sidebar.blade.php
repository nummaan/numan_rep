
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
.l_f {
    left: 0;
    right: auto;
}
.l_e {
    -webkit-overflow-scrolling: touch;
    bottom: 0;
    overflow-x: hidden;
    overflow-y: auto;
    top: 0;
    -webkit-transition: -webkit-transform .5s ease 0s;
    transition: -webkit-transform .2s ease 0s;
    transition: transform .5s ease 0s;
    transition: transform .5s ease 0s,-webkit-transform .5s ease 0s;
    width: 320px;
    position: absolute;
}

.x_a {
    background: #fff;
    -webkit-box-shadow: 0 1px 3px 0 rgba(0,0,0,.07), 0 3px 13px 0 rgba(0,0,0,.16);
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.07), 0 3px 13px 0 rgba(0,0,0,.16);
}
.e_f {
    flex-direction: column;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
}

.e_a {
    display: -webkit-box;
    display: flex;
}
    </style>
  </head>
  <body>
      <nav class="nav navbar navbar-laravel">
          <li class="nav-item">
              <i onclick="w3_open()" class="fas fa-bars fa-2x"></i>
          </li>
      </nav>

    <div id="mySidebar" class="x_a e_a l_e l_f e_f">
        <div class="container-fluid py-3">
            <div class="row">
                <div class="col-3">
                    <img src="{{ asset('uploads/avatars/1579236615.png') }}" alt="logo" class="rounded w-100 h-auto">
                    
                </div>
                <div class="col-7">
                    <h5 class="mt-2">Website Name</h5>
                </div>
                <div class="col-2 text-right">
                    <i  onclick="w3_close()" class="mt-3 fas fa-times"></i>
                </div>
            </div>
            <ul>
                <li>Home</li>
                <li>About</li>
                <li>Logout</li>
            </ul>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    <script>
        function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        }
        
        function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        }
    </script>
</body>
</html>

