<div id="menu" class="border border-white p-2 bg-white" style="border-width:2px !important;">
    <ul class="">
        <li class="extras-item"><a class="dropdown-item" href="{{route('aboutus')}}" style="text-decoration: none;">About Us</a></li>
        </li>
        <li class="extras-item"><a class="dropdown-item" style="text-decoration: none;" href="{{ url('/privateChat/'.Auth::id().',55'.'/'.Auth::id().'/0') }}">Help Desk</a></li>
         <li class="extras-item"><a class="dropdown-item" href="{{ route('faquser') }}" style="text-decoration: none;">FAQ</a></li>
        <li class="extras-item"><a class="dropdown-item" href="/dispute" style="text-decoration: none;">Dispute</a></li>
        <li class="extras-item"><a class="dropdown-item" href="#" style="text-decoration: none;">Admin Menu</a></li>
        <?php
        if (Auth::user()) {
        $login_user_id = Auth::user()->id;
        $users_manus = DB::table('menu_options')
            ->join('user_menu', 'user_menu.menu_options_id', '=', 'menu_options.id')
            ->where('menu_options.id', "!=", 14)
            ->where('menu_options.name', "!=", "Blog Admin")
            ->where('menu_options.name', "!=", "Event Admin")
            ->where('menu_options.name', "!=", "Category Setup")
            ->where('menu_options.name', "!=", "More Admin")
            ->where('menu_options.name', "!=", "Dispute Manager")
            ->where('menu_options.name', "!=", "Bid Admin")
            ->where('menu_options.name', "!=", "Training Setup")
            ->where('menu_options.name', "!=", "Exam Setup")
            ->where('menu_options.name', "!=", "Upcoming Services")
            ->where('menu_options.name', "!=", "HelpDesk Agent")
            ->where('user_menu.user_id', $login_user_id)->get();
            $help_desk_label = '';
            $help_desk_link = '';
             foreach ($users_manus as $users_manu){
                    if($users_manu->name == 'HelpDesk'){
                        $help_desk_label = $users_manu->name;
                        $help_desk_link = $users_manu->link;
                    }else{
                        ?>
                        <li class="extras-item dropdown-submenu">
                            <ul>
                                <li class="extras-item"><a class="dropdown-item" href="{{ url($users_manu->link) }}"><?php echo $users_manu->name; ?></a></li>
                            </ul>
                            
                        </li>
                        <?php
                    }
                }
            }
            ?>
        <li class="extras-item"><a class="dropdown-item" href="{{ URL('/upcoming_services') }}" style="text-decoration: none;">Upcoming Services</a></li>
       <li class="extras-item ml-4">
            <a class="dropdown-item" href="{{ route('login') }}"
               data-toggle="modal" data-target="#filter-modal">
                <i class="fas fa-sliders-h fa-1x"></i>
                Filters
            </a>
            <a class="dropdown-item" href=" {{ route('demopage')}}">
               Demo Page
            </a>
            <a class="dropdown-item" href=" {{ route('home2')}}">
                Home 2
            </a>
            <a class="dropdown-item" href="{{ route('newdashboard') }}">newdashboard</a>
        </li>
        <li class="extras-item ml-4 dropdown-submenu">
            <!-- <i class="fas fa-plus-circle fa-1x"></i> Post</span> -->
            <ul>
                <!-- <li class="extras-item"><a class="dropdown-item" href="{{ route('buyer.create') }}">
                    Want to buy
                </a></li>
                <li class="extras-item"><a class="dropdown-item" href="{{ route('seller.create') }}">
                    Want to Sell
                </a></li>
                <li class="extras-item"><a class="dropdown-item" href="{{ route('article.create') }}">
                    Article Post
                </a></li>
                <li class="extras-item"><a class="dropdown-item" href="{{ route('AdvertisementPage') }}">
                    Advertisement
                </a></li> -->
                <li class="extras-item"><a class="dropdown-item" href="{{ route('coupons') }}">
                    Coupon
                </a></li>
                <li class="extras-item"><a class="dropdown-item" href="{{ url('CategorySetup') }}">
                    Category Setup
                </a></li>
                
                <li class="extras-item">
                    <a class="dropdown-item" href="{{ route('trainuser') }}">
                        Training
                    </a></li>
                <li class="extras-item">
                    <a class="dropdown-item" href="{{ url('/trainsetup') }}">
                        Training Setup
                    </a></li>
                <li class="extras-item">
                    <a class="dropdown-item" href="{{ route('examuser') }}">
                        Exam
                    </a></li>
                    <li class="extras-item">
                    <a class="dropdown-item" href="{{ url('/examsetup') }}">
                        Exam Setup
                    </a></li>
            </ul>
        </li>
    </ul>
    <div>

        {{-- <ul class="list-group text-secondary">
            @php
                $menu_options = \App\Menu_option::all();
            @endphp
            @foreach ($menu_options as $menu_option)
            <li class="list-group-item">
                <a class="text-secondary" href="{{ url($menu_option->link) }}">{{$menu_option->name}}</a>
            </li> --}}

            {{-- @endforeach
        </ul> --}}

    </div>
</div>