<div id="chat" class="border border-white p-2" style="border-width:2px !important;">

    {{-- Chat begins --}}

    <div class="topleftchatdiv" >
        <div>
            <input type="text" name="" placeholder="&#xf002; Search" id="leftnav-search" class="form-control"
                style="font-family: FontAwesome, 'Open Sans', Verdana">
        </div>
        <table class="table table-bordered table-hover text-success" style="background: white;width: 95%;">
            <tbody id="tbodChat">
            </tbody>
        </table>
        <div>
            <select id="chattype" class="custom-select" style="font-family: FontAwesome, 'Open Sans', Verdana"
                v-model="chattype">
                <option selected>All Conversation <span><i class="fa fa-caret-down"></i></span></option>
                <option value="Unread">
                    Unread
                </option>
                <option value="Archieved">
                    Archieved
                </option>
                <option value="Spam">
                    Spam
                </option>
                <option value="Report">
                    Report
                </option>
            </select>
    
        </div>
        {{-- <input type="text" placeholder="Search with name" class="form-control search-box" id="search" name="search" autocomplete="off">
        <a class="btn dropdown-toggle alink" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="true">
        All conversation
        </a>
        <div class="dropdown-menu">
           <ul>
              <li class="dropdown-item "  id="unrea" aria-expanded="true" >Unread</li>
              <li class="dropdown-item" href="#">Archive</li>
              <li  class="dropdown-item "  aria-expanded="true" id="sss">Spam</li>
              <li class="dropdown-item" id="report" aria-expanded="true">Report</li>
           </ul>
           <div id="lll">
           </div>
        </div> --}}
        {{-- spam body --}}
        <div id="spambody" >
        </div >
        {{-- unread body --}}
        <div id="unread" >
        </div>
        {{--report body --}}
        <div id="reportbody" >
        </div>
        {{-- level --}}
        {{--
        <div id="indeviduallevelsearch">
        </div>
        --}}
     </div>
     <div class="chat-members bg-white text-secondary">
        <ul class="list-group" >
            {{-- This change is done reguarding a issue of passing variable related --}}
            {{-- TODO: It should be fixed later --}}
            @auth
                @php
                    $userid = Auth::user()->id;
                    $messages = [];
                    $helpDeskUser = 0;
                @endphp
                @foreach(receivers(Auth::user()->id) as $receiver)
                @php
                $receiverid = $receiver->id;
                if($userid==$receiverid){
                continue;
                }
                if($receiverid > $userid){
                $chatRoomId = $userid.','.$receiver->id;
                }
                else{
                $chatRoomId = $receiver->id.','.$userid;
                }
                $romid=App\Chatroom::where('chatRoomId',$chatRoomId)->first();
                if(empty($romid)){
                $chat = new App\Chatroom;
                $chat->chatRoomId = $chatRoomId;
                $chat->save();
                $romid = Chatroom::where('id', $chat->id)->first();
                }
                $romid=$romid->id;
                $messagecont=App\Message::where('RoomId',$romid)
                ->where('readWriteStatus','!=',1)
                ->where('sender','!=',$userid)
                ->count();

                $starlevel=App\Level::where('userleveler',$userid)
                ->where('userbeenleveled',$receiver->id)
                ->where('value','Star')->count();

                $message='';
                if($messagecont>0){
                $messageunread = App\Message::where('RoomId',$romid)
                ->where('readWriteStatus','!=',1)
                ->where('sender','!=',$userid)
                ->orderBy('created_at','DESC')
                ->first();
                if($messageunread) {
                    $messageunread->setAttribute('messagecont', $messagecont);
                    $messageunread->setAttribute('chatRoomId', $chatRoomId);
                    $messageunread->setAttribute('receiver', $receiver);
                    $messageunread->setAttribute('starlevel', $starlevel);
                    $messageunread->setAttribute('messageunread', 0);
                }
                array_push($messages, $messageunread);
                }
                else{
                $message = App\Message::where('RoomId',$romid)->orderBy('created_at','DSEC')->first();
                $messageunread=0;
                if($message) {
                    $message->setAttribute('messagecont', $messagecont);
                    $message->setAttribute('chatRoomId', $chatRoomId);
                    $message->setAttribute('receiver', $receiver);
                    $message->setAttribute('starlevel', $starlevel);
                    $message->setAttribute('messageunread', 0);
                }
                array_push($messages, $message);
                }

                //echo $timestring;

                //I DON'T KNOW WHAT IS HELP DESK IS I AM ASSIGNING HELPDESK TO 0 IF THERE IS NO HELPDESK SET THE CODE MAY HAVE TO CHANGE
                /*if(!isset($helpDeskUser)) {
                    $helpDeskUser = 0;
                }*/
                //=======================================================================================================================

                @endphp

                @endforeach
                @php
                    usort($messages, function($a, $b) {
                      return ($a['created_at'] < $b['created_at']) ? 1 : -1;
                    });
                @endphp
                @foreach($messages as $message)
                    @php
                        $_messageunread = $message ? $message->messageunread : 0;
                        $_messagecont = $message ? $message->messagecont : 0;
                        $_chatRoomId = $message ? $message->chatRoomId : 0;
                        $_receiver = $message ? $message->receiver : null;
                        $timestring=' ';
                        //dd($message);
                        if($message!=''){
                        $time = strtotime($message->created_at);
                        $date= date('Y-m-d H:i:s', $time);
                        $date = date_create($date);
                        $nowdate = date("Y-m-d H:i:s");
                        $nowdate = date_create($nowdate);
                        $diff = date_diff($nowdate, $date);
                        if($diff->s > 0 && $diff->i <1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0){
                        $timestring='just now';
                        }elseif ($diff->i >=1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                        $timestring=$diff->i.'m  ago';
                        }elseif ($diff->i >=1  && $diff->h >= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                        $timestring=$diff->h.'h    '.$diff->i.'m ago';
                        }elseif($diff->h >= 0 && $diff->d >=0 && $diff->m <=0 && $diff->y <=0){
                        $timestring=$diff->d.'d  '.$diff->h.'h ago';
                        }elseif($diff->d >=0 && $diff->m >=0 && $diff->y <=0){
                        $timestring=$diff->m.'months  '.$diff->d.'days ago';
                        }
                        elseif ($diff->m >=0 && $diff->y >=0) {
                        $timestring=$diff->y.' year '.$diff->m.' months ago';
                        }
                        }
                        if($_messageunread){
                        $time = strtotime($message->created_at);
                        $date= date('Y-m-d H:i:s', $time);
                        $date = date_create($date);
                        $nowdate = date("Y-m-d H:i:s");
                        $nowdate = date_create($nowdate);
                        $diff = date_diff($nowdate, $date);
                        if($diff->s > 0 && $diff->i <1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0){
                        $timestring='just now';
                        }elseif ($diff->i >=1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                        $timestring=$diff->i.'m  ago';
                        }elseif ($diff->i >=1  && $diff->h >= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                        $timestring=$diff->h.'h  '.$diff->i.'m ago';
                        }elseif($diff->h >= 0 && $diff->d >=0 && $diff->m <=0 && $diff->y <=0){
                        $timestring=$diff->d.' days  '.$diff->h.'h ago';
                        }elseif($diff->d >=0 && $diff->m >=0 && $diff->y <=0){
                        $timestring=$diff->m.' months '.$diff->d.' days ago';
                        }
                        elseif ($diff->m >=0 && $diff->y >=0) {
                        $timestring=$diff->y.' year '.$diff->m.' months ago';
                        }
                        }
                    @endphp
                    @if($_messageunread)

                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{asset('/uploads/avatars/'.$_receiver->avatar)}}" alt="person"
                                         class="w-100 h-auto rounded-circle">
                                    @if($_receiver->onlineStatus==1)
                                        <span class="badge badge-success">online</span>
                                    @else
                                        <span class="badge badge-warning">away</span>
                                    @endif
                                </div>
                                <div class="col-9">
                                    <div class="row">
                                        <a href="{{url('privateChat/'.$_chatRoomId.'/'.$userid.'/'.$helpDeskUser)}}"><span class="text-dark font-weight-bold">{{$_receiver->name}}</span></a>
                                        @if($starlevel !=0)
                                            <span><i class="far fa-star text-warning">{{$timestring}}</i></span>
                                        @else
                                            <span><i class="far fa-star">{{$timestring}}</i></span>

                                        @endif
                                    </div>
                                    <div class="row">
                                        <h5 class="col-md-10 well-sm message_font" style="display:inline;" ><b>{{ $message->message}}</b></h5>
                                        <span class="badge badge-success">{{$_messagecont}}</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @elseif($message)

                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{asset('/uploads/avatars/'.$_receiver->avatar)}}" alt="person"
                                         class="w-100 h-auto rounded-circle">
                                    @if($_receiver->onlineStatus==1)
                                        <span class="badge badge-success">online</span>
                                    @else
                                        <span class="badge badge-warning">away</span>
                                    @endif
                                </div>
                                <div class="col-9">
                                    <div class="row">
                                        <a href="{{url('privateChat/'.$_chatRoomId.'/'.$userid.'/'.$helpDeskUser)}}"><span class="text-dark">{{$_receiver->name}}</span></a>
                                        @if($starlevel !=0)
                                            <span><i class="far fa-star text-warning">{{$timestring}}</i></span>
                                        @else
                                            <span><i class="far fa-star">{{$timestring}}</i></span>

                                        @endif
                                    </div>
                                    <div class="row">
                                        <h5 class="col-md-10 well-sm message_font" style="display:inline;" ><b>{{ $message->message}}</b></h5>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endif
                @endforeach
            @endauth

        </ul>
     </div>
 

    {{-- chat ends --}}

</div>
@push('footerjs')
<script>
    // var app = new Vue({
    //             el: '#chat',
    //             data: {
    //                 chattype: ''
    //             },
    //
    //             methods: {
    //
    //             }});
</script>
@endpush