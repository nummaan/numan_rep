<style>
    #chat, #chathelpdesk, #post, #menu {
        display: none;
    }
    .border-orange {
        border: 3px orangered solid !important;
    }
    .sidenav {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        overflow-x: hidden;
        transition: 0.5s;
    }
</style>
<div id="mySidenav" class="sidenav bg-dark text-white position-absolute" style="z-index:1100;">
    <?php
    $helpDeskUser = 0;
    use Illuminate\Support\Facades\DB;
    $site_info = DB::table('site_info')->get();
    $info_element_array = array();
    foreach ($site_info as $info_element) {
        $info_element_array[$info_element->attr_name] = $info_element->attr_value;
    }
    ?>
    <div class="container-fluid">
        <div class="py-3">
            <img src="{{ URL::asset('uploads/avatars/'.$info_element_array['header_left_pic']) }}" class="rounded" alt="logo" style="wdith:3rem; height:3rem;">
            <span class="font-weight-bold">{{$info_element_array['site_name']}}</span>
            <span onclick="closeNav()"><i class="fas fa-times float-right mt-1"></i></span>
        </div>
        <hr class="bg-white mt-1">
        <div class="mb-3 text-center">
            @auth
            @if (Auth::user()->avatar)
                <a id="navbarDropdown" class="d-inline text-white text-decoration-none" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false" v-pre
                   >

                    <a href="{{url('/userprofile')}}" class=" text-white text-decoration-none"> <img class="rounded-circle mx-1" src="{{ asset('/uploads/avatars/' . Auth::user()->avatar) }}"
                                                            style="width:3rem; height:3rem;">{{ Auth::user()->name }}
                    </a>
                    <span class="caret"></span>
                </a>
            @else
                <a id="navbarDropdown" class="d-inline text-white text-decoration-none" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false" v-pre
                   >

                    <img class="rounded-circle mx-1" src="{{ asset('img/default.png') }}"
                         style="width:3rem; height:3rem;">
                         {{ Auth::user()->name }}
                    <span class="caret"></span>
                </a>
            @endif
            @endauth
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <!--
                <a class="dropdown-item" href="{{ route('trainuser') }}">
                    Training
                </a>
                <a class="dropdown-item" href="{{ route('examuser') }}">
                    Exam
                </a> -->
                <!-- <a class="dropdown-item" href="{{ route('settings') }}">
                    Settings
                </a> -->

                <!-- <a class="dropdown-item" href="{{ route('faq.category.index') }}">
                    FAQ Category
                </a> -->
                <?php
                if (Auth::user()) {
                $login_user_id = Auth::user()->id;
                $users_manus = DB::table('menu_options')
                    ->join('user_menu', 'user_menu.menu_options_id', '=', 'menu_options.id')
                    ->where('menu_options.id', "!=", 14)
                    ->where('menu_options.name', "!=", "Blog Admin")
                    ->where('menu_options.name', "!=", "Event Admin")
                    ->where('menu_options.name', "!=", "Category Setup")
                    ->where('menu_options.name', "!=", "More Admin")
                    ->where('menu_options.name', "!=", "Dispute Manager")
                    ->where('menu_options.name', "!=", "Bid Admin")
                    ->where('menu_options.name', "!=", "Training Setup")
                    ->where('menu_options.name', "!=", "Exam Setup")
                    ->where('menu_options.name', "!=", "Upcoming Services")
                    ->where('user_menu.user_id', $login_user_id)->get();
                    $help_desk_label = '';
                    $help_desk_link = '';
                     foreach ($users_manus as $users_manu){
                            if ($users_manu->name == "HelpDesk Agent") {
                                $helpDeskUser = 1;
                            }
                            if($users_manu->name == 'HelpDesk'){
                                $help_desk_label = $users_manu->name;
                                $help_desk_link = $users_manu->link;
                            }else{
                                ?>
                                <a class="dropdown-item" href="{{ url($users_manu->link) }}"><?php echo $users_manu->name; ?></a>
                                <?php
                            }
                        }
                    }
                    ?>

                <!-- <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a> -->

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <div class="container-fluid d-md-none d-block">
            <div class="row text-center">
                <div class="col-md-4 pr-0 pt-2 mb-3">
                    Showing 1 to 10 of 30 entries
                </div>
                <div class="col-md-1 pl-0 pt-2 mb-3">
                    <i id="listshow" class="fas fa-th-list fa-2x pr-1"></i>
                    <i id="gridshow" class="fas fa-th fa-2x pl-1"></i>
                </div>
                <div class="col-md-7 mb-3">
                    <nav class="nav">
                        <span class="mt-2 px-2">
                            Sort By:

                        </span>
                        <li class="nav-item mb-3">
                            <select class="custom-select" style="width:140px;">
                                <option selected>Price</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </li>
                        <li class="nav-item border border-black-50">
                            <a class="nav-link text-black-50" href="#">Popularity</a>
                        </li>
                        <li class="nav-item border border-black-50">
                            <a class="nav-link text-black-50" href="#">Latest</a>
                        </li>
                        <li class="nav-item border border-black-50">
                            <a class="nav-link text-black-50" href="#">Rating</a>
                        </li>
                    </nav>
                </div>
            </div>
        </div>
        <div class="bg-white text-dark rounded mb-3 py-1">
            <nav class="nav">
                <li class="nav-item px-3">
                    <i class="fas fa-sliders-h"></i>
                </li>
                <li class="nav-item px-3">
                    <i class="far fa-comments"></i>
                </li>
                <li class="nav-item px-3">
                    <i class="fas fa-plus-circle"></i>Post
                </li>
                <li class="nav-item px-3">
                    <i class="fas fa-ellipsis-v"></i>
                </li>
                <li class="nav-item px-3">
                    <i class="fas fa-sign-out-alt"></i>
                </li>
            </nav>
        </div>
        @if($helpDeskUser == 1)
            <div id="chat_switcher" class="chat-switcher">
                <a id="chat-member" href="#">Member</a>
                <a id="chat-agent" href="#">Agent</a>
            </div>
        @endif
        <div>
            @include('sidebars.filter')
            @include('sidebars.chat')

            @if ($helpDeskUser == 1)
                @include('sidebars.chat_helpdesk')
            @endif

            @include('sidebars.post')
            @include('sidebars.menu')
        </div>

    </div>
</div>
@push('footerjs')
    <script>
        function navChatChange (action_on, action_for) {
          $(action_on).click(function () {
            $(action_for).siblings().hide();
            $(action_for).show();
          });
        }

        function navChange ( action_on , action_for ) {
            const chatSwitcher = $("#chat_switcher");
            chatSwitcher.hide();

            $(action_on).parent().click(function () {
              if (action_on === ".fa-comments") {
                chatSwitcher.show();
              } else {
                chatSwitcher.hide();
              }
              $(this).siblings().removeClass('border-orange');
              $(this).addClass('border-orange');
              $(action_for).siblings().hide();
              $(action_for).show();
            });
        }
        function openNav() {
            document.getElementById("mySidenav").style.width = "310px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
        $(document).ready( function () {
            navChange('.fa-sliders-h','#filter');
            navChange('.fa-comments','#chat');

            navChatChange('#chat-member','#chat');
            navChatChange('#chat-agent','#chathelpdesk');

            navChange('.fa-plus-circle','#post');
            navChange('.fa-ellipsis-v','#menu');
            // $('.fa-times').click(function () {

            //     $('#sidebar').hide();
            //     $('#left-sidebar').css({'width':'12%','transition': 'width .5s'});
            //     $('#main-content').css({'width':'74%','transition': 'width .5s'});
            //  });
            //  $('#opensidebar').click(function () {
            //     $('#sidebar').show();
            //     $('#left-sidebar').css({'width':'23%','transition': 'width .5s'});
            //     $('#main-content').css({'width':'65%','transition': 'width .5s'});
            //  });

        });

    </script>
@endpush