<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@if ($setting = \App\Setting::first())
    <meta name="pusher_app_key" content='{{ $setting->pusher_app_key }}'>
    <meta name="pusher_app_cluster" content='{{ $setting->pusher_app_cluster }}'>
@endif
<meta property="og:image:width" content="1381">
<meta property="og:image:height" content="723">
<meta property="og:description" content="Description here">
<meta property="og:url" content="http://splashthemepark.com/home">
<meta property="og:image" content="http://yousoftme.com/storage/app/public/slider/4.jpg?fbclid=IwAR33FGMP7QRGi1A0Hb0wOWMizbDo-41lFjU3Z-ACcXtY1CN5FI_zMB2ok2E">
<meta property="og:title" content="title here">
<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/post_layout.css') }}">
<link rel="stylesheet" href="{{ asset('css/summernote.min.css') }}">
@include('style.internal')
@include('partials.head')
@php
    $site_info = \DB::table('site_info')->get();
    $info_element_array = array();
    foreach ($site_info as $info_element) {
        $info_element_array[$info_element->attr_name] = $info_element->attr_value;
    }
    $currentPage = 'otherpages';
    $adsUserId = 0;
    $popupAds = \App\library\SiteHelper::getPopupAds($currentPage,$adsUserId);

    $leftAds = \App\library\SiteHelper::getLeftAds($currentPage ,$adsUserId);
    $rightAds = \App\library\SiteHelper::getRightAds($currentPage ,$adsUserId);
    $addcls = 'col-lg-2 col-md-2';
    $contCls =' col-lg-6 col-md-6';
    if (isset($homeList)) {
        if ($setting->view_style !== 'facebook') {
            $addcls = 'col-lg-2 col-md-1';
            $contCls =' col-lg-8 col-md-10';
        }
    } else {
        $addcls = 'col-lg-2 col-md-2';
        $contCls =' col-lg-8 col-md-8';
    }
@endphp
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&autoLogAppEvents=1&version=v6.0&appId=558038808129662"></script>
@include('partials.ModelPopup')
@include('partials.second_nav')
<div class="position-relative main-page-container">
    @include('sidebars.new-sidebar')
    @include('partials.nav')
    @if($popupAds->count() > 0)
        <div id="open-modal" class="modal-window modal-sm" style="display: block;padding:0px;">
            <div>
                <div class="closebtnarea" align="right"><i title="Close" onclick="$('#open-modal').hide('slow');" class="fa fa-times-circle fl-r crs-pntr" style="font-size:27px;color:#6c757d;"></i></div>
                <br>
                <div class="container">
                    <div class="">
                        @foreach($popupAds as $popupAd)
                        <div class="col-md-12 margin-bottom-15" style="padding-right: 10px;padding-bottom: 10px;padding-left: 10px;">
                            <div class="adsContent">
                                @if($popupAd->adds_type == 'image')
                                <a class="adsLink" href="{{ 'http://'.$popupAd->image_link }}" target="_blank">
                                    <img src="{{ asset('/uploads/adsImages/'.$popupAd->image) }}" class="adsImage" style="height:300px;border-radius: 10px;">
                                </a>
                                @elseif($popupAd->adds_type == 'embed_code')
                                <?php echo $popupAd->embed_code ?>
                                @elseif($popupAd->adds_type == 'referral_code')
                                {{ $popupAd->referral_code }}
                                @else
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div style="position:fixed; right:0%; bottom:0%;z-index: 2;">
            <div id="messagepop"></div>
        </div>
    @endif
    <div class="">
        <div class="main-cont row <?php echo (isset($homeList) ? 'cont-home-page' : 'cont-other-page')  ?> <?php echo $setting->view_style == 'facebook' ? 'root-home-facebook' : 'root-home-pinterest' ?>">
            @if (isset($homeList) && $setting->view_style == 'facebook' )
            <div class="col-lg-1 col-md-1 d-none d-md-block"></div>
            @endif
            <div class="cls-left-root d-none d-md-block {{$addcls}}" id="left-add-root">
                <div class="cls-add-row sticky-top ">
                    @foreach ($leftAds as $value)
                    <div class="cls-left-add-block">
                        <a href="{{ ($value->image_link ) ? $value->image_link : '#' }}"
                            {{ ($value->image_link) ? 'target="_blank"' : '' }} class="add-link">
                            <div class="cls-left-add-img">
                                <img class="" src="{{$value->image? asset("uploads/adsimages/".$value->image):asset("images/image_not_found.jpg")}}" width="200px" alt="Card image cap">
                            </div>
                            <div class="cls-left-add-title">
                                <span class="">{{$value->adds_name}} </span>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="cls-containt-rooot {{$contCls}} col-sm-12 ">                
                @if (!isset($homeList))
                    @php                
                        $topAdsot = \App\library\SiteHelper::getTopAds($currentPage ,$adsUserId)->toArray(); 
                    @endphp            
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row justify-content-center ">                           
                                    <div class="col-md-12 cls-bottom-add-item ">
                                         @yield('content')   
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>            
                @endif           
            </div>
            <div clas="cls-right-add-root-new d-none d-md-block d-none d-md-block {{$addcls}}" id="right-add-root" style="float: right;width: 200px;">            
                <div class="cls-add-row sticky-top">
                    @foreach ($rightAds as $value)
                    <div class="cls-right-add-block">
                        <a href="{{ ($value->image_link ) ? $value->image_link : '#' }}" {{ ($value->image_link) ? 'target="_blank"' : '' }} class="add-link">
                            <div class="cls-right-add-img">
                                <img class="" src="{{$value->image? asset("uploads/adsimages/".$value->image):asset("/images/image_not_found.jpg")}}" alt="Card image cap">
                            </div>
                            <div class="cls-left-add-title">
                                <span class="">{{$value->adds_name}} </span>
                            </div>                            
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>    
        </div>
        @if (!isset($homeList))
        @php
            $botAdsot = \App\library\SiteHelper::getBottomAds($currentPage ,$adsUserId)->toArray();
            $topAdsot = \App\library\SiteHelper::getTopAds($currentPage ,$adsUserId)->toArray();
            $allAds = array_merge( $botAdsot);
            $allAdsCount = count($allAds);
            $addCnt = 0;
        @endphp 
        @endif
       
        <!-- Scroll top buttm -->
        <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
        <script>
            mybutton = document.getElementById("myBtn");

            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function () {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    mybutton.style.display = "block";
                } else {
                    mybutton.style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
                return false;
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            }
        </script>    
        <!-- Scroll top buttm -->
        <div class="footer d-none  cls-footer">
            <div class="row  cls-social">
                <div class="col-md-12 social-section">
                    <ul class="footer-social">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
            </div>
            @if(isset($controller))
            <div class=" adsMargin">
                <div class="">
                    <div class="exampleSlider">
                        <div class="MS-content">
                            @php
                            $bottomAds = \App\library\SiteHelper::getBottomAds($currentPage,$adsUserId);
                            $countBottomAdds = (sizeof($bottomAds));
                            @endphp
                            @foreach($bottomAds as $bottomAd)
                            <div class="item">
                                <a href="{{ 'http://'.$bottomAd->image_link }}"><img height="50px"
                                        style="max-height:100px;height:100px"
                                        src="{{ asset('/uploads/adsImages/'.$bottomAd->image) }}"
                                        class="img-responsive"></a>
                                <span>{{$bottomAd->adds_name}} </span>
                            </div>
                            @endforeach
                            <div class="MS-controls">
                                <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                                <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        </div>
        <div style="position:fixed; right:0%; bottom:0%;z-index:2">
            <div id="messagepop"></div>
        </div>
    </div>
    @include('partials.footer')
    @include('partials.filter-modal')

    @include('partials.JS')
    @include('partials.ModelPopupJs')   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script>
        var idleTime = 0;
        $(document).ready(function () {
            // stiky();
            // stikyRight();
            //Increment the idle time counter every minute.
            var idleInterval = setInterval(timerIncrement, 1000); // 1 minute

            //Zero the idle timer on mouse movement.
            $(this).bind('mousewheel', function (e) {
                idleTime = 0;
                $('.cls-footer').addClass('d-none');
            });
            $(this).mousemove(function (e) {
                // idleTime = 0;
                // $('.cls-footer').addClass('d-none');
            });
            $(this).keypress(function (e) {
                $('.cls-footer').addClass('d-none');
                idleTime = 0;
            });
        });

        function timerIncrement() {
            idleTime = idleTime + 1;
            if (idleTime > 3) { // 
                $('.cls-footer').removeClass('d-none');
            }
        }
    </script>
    @stack('footerJs')
    @stack('footerjs')
    <script>
        $(document).ready(function () { 
            $('.fa-th-list').click(function () { 
                $('#grid-display').hide();
                $('#list-display').show();
             });
             $('.fa-th').click(function () { 
                $('#list-display').hide();
                $('#grid-display').show();
             });
         });
    </script>
    </body>
</html>