@extends('layouts.app')
@php

$buyers = \App\Buyer::orderBy('id', 'desc')->where(function($q) {
                    $q->where(DB::raw("DATE_ADD(created_at,INTERVAL hour HOUR)"), '>', now());
                })->paginate(10);
        //$sellers = Seller::orderBy('id', 'desc')->paginate(10);
        $sellers = \App\Seller::orderBy('id', 'desc')->where(function($q) {
                    $q->where(DB::raw("DATE_ADD(created_at,INTERVAL hour HOUR)"), '>', now());
                })->paginate(10);
        $articles = \App\Article::orderBy('id', 'desc')->paginate(10);
        //get all published events 
        $events = \App\Event::where('is_published', '=', 'yes')->orderBy('id', 'desc')->get();
        $posts = \App\Blog::latest()->paginate(5);
        $setting = \App\Setting::first();
        $homeList = true;

$leftAds = \App\library\SiteHelper::getLeftAds('otherpages',0);
$rightAds = \App\library\SiteHelper::getRightAds('otherpages',0);
$allAds = array_merge($rightAds->toArray(),$leftAds->toArray());
$allAdsCount = count($allAds);
$addCnt = 0;

$tot_posts = count($buyers) + count($sellers) + count($articles) + count($posts);
$totLedtAdds = count($leftAds);
$totrightAds = count($rightAds);
$post_col = 'col-md-3';
$repPrRow = ($totLedtAdds > 0) ? round( 6 / $totLedtAdds) : 0;
$repPrRowR = $totrightAds > 0 ? ceil( 4 / $totrightAds) :0 ;
$rows = ceil($tot_posts / 4 );
if($setting->view_style == 'facebook'){
$post_col = ' col-md-12 col-lg-12';
$repPrRow = ($totLedtAdds > 0) ? ceil( 5 / $totLedtAdds) : 0;
$repPrRowR = $totrightAds > 0 ? ceil( 4 / $totrightAds) : 0;
$rows = ceil($tot_posts / 1 );
}
@endphp
@section('sidebar')
@include('sidebars.ysfsidebar')
    
@endsection
@section('content')
<div id="main-content" class="p-3 col-12">
    <form action="" method="post">
        <input type="text" class="form-control" placeholder="&#xf002; Search" style="font-family: FontAwesome, 'Open Sans', Verdana">
    </form>

    @if($error = Session::get('error'))
        <div class="alert alert-danger">
            <div class="alert-title"><h5>OOPS !!!</h5></div>
            {{ $error }}
        </div>    
    @endif
    @if($success = Session::get('success'))
        <div class="alert alert-success">
            <div class="alert-title"><h5>Success!</h5></div>
            {{ $success }}
        </div>  
    @endif
    <div class="container-fluid d-md-block d-none">
        <div class="row">
            <div class="col-4 pr-0 pt-2">
                Showing 1 to 10 of 30 entries
            </div>
            <div class="col-1 pl-0 pt-2">
                <i id="listshow" class="fas fa-th-list pr-1"></i>
                <i id="gridshow" class="fas fa-th pl-1"></i>
            </div>
            <div class="col-7">
                <nav class="nav">
                    <span class="mt-2 px-2">
                        Sort By:

                    </span>
                    <li class="nav-item">
                        <select class="custom-select" style="width:140px;">
                            <option selected>Price</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </li>
                    <li class="nav-item border border-black-50">
                        <a class="nav-link text-black-50" href="#">Popularity</a>
                    </li>
                    <li class="nav-item border border-black-50">
                        <a class="nav-link text-black-50" href="#">Latest</a>
                    </li>
                    <li class="nav-item border border-black-50">
                        <a class="nav-link text-black-50" href="#">Rating</a>
                    </li>
                </nav>
            </div>
        </div>
    </div>
    <div id="grid-display">
        @include('posts.grid')
    </div>
    <div id="list-display" style="display:none;">
        @include('posts.list')
    </div>
</div>
@endsection
