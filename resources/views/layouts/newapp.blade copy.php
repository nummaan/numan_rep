<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @if ($setting = \App\Setting::first())
        <meta name="pusher_app_key" content='{{ $setting->pusher_app_key }}'>
        <meta name="pusher_app_cluster" content='{{ $setting->pusher_app_cluster }}'>
    @endif
    <meta property="og:image:width" content="1381">
    <meta property="og:image:height" content="723">
    <meta property="og:description" content="Description here">
    <meta property="og:url" content="http://splashthemepark.com/login">
    <meta property="og:image" content="http://localhost/test/public/uploads/avatars/logo.png">
    <meta property="og:title" content="title here">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Car Bidder</title>

    @include('style.internal')
    @include('partials.head')
  </head>
  <body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&autoLogAppEvents=1&version=v6.0&appId=558038808129662"></script>
    
    @include('partials.second_nav')
    <div class="position-relative">
        @include('sidebars.ysfsidebar')
        @include('partials.nav')
        <div class="container-fluid">
            <div class="row">
                @include('advertise.left')
                

                <div id="main-content" class="p-3 col-8">
                    <form action="" method="post">
                        <input type="text" class="form-control" placeholder="&#xf002; Search" style="font-family: FontAwesome, 'Open Sans', Verdana">
                    </form>
                    <div>
                        <div class="alert alert-danger">
                            <div class="alert-title"><h5>OOPS !!!</h5></div>
                        
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fugit iusto, a esse quo sapiente voluptates eius illo vero hic reiciendis quis quidem tenetur ducimus, tempora eum nam, alias nihil deserunt.
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-4 pr-0 pt-2">
                                Showing 1 to 10 of 30 entries
                            </div>
                            <div class="col-1 pl-0 pt-2">
                                <i id="listshow" class="fas fa-th-list pr-1"></i>
                                <i id="gridshow" class="fas fa-th pl-1"></i>
                            </div>
                            <div class="col-7">
                                <nav class="nav">
                                    <span class="mt-2 px-2">
                                        Sort By:

                                    </span>
                                    <li class="nav-item">
                                        <select class="custom-select" style="width:140px;">
                                            <option selected>Price</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </li>
                                    <li class="nav-item border border-black-50">
                                        <a class="nav-link text-black-50" href="#">Popularity</a>
                                    </li>
                                    <li class="nav-item border border-black-50">
                                        <a class="nav-link text-black-50" href="#">Latest</a>
                                    </li>
                                    <li class="nav-item border border-black-50">
                                        <a class="nav-link text-black-50" href="#">Rating</a>
                                    </li>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div id="grid-display">
                        @include('posts.grid')
                    </div>
                    <div id="list-display" style="display:none;">
                        @include('posts.list')
                    </div>
                </div>
                @include('advertise.right')
            </div>
        </div>
    </div>
    @php
$currentPage = 'otherpages';
$adsUserId = 0;
@endphp
@if ($controller == 'UserController')
@php
$currentPage = 'profile';
$adsUserId = $user->id;
@endphp
@endif
@php

$popupAds = \App\library\SiteHelper::getPopupAds($currentPage,$adsUserId);
@endphp
@if($popupAds->count() > 0)
<div id="open-modal" class="modal-window modal-sm" style="display: block;padding:0px;">
    <div>
        <div class="closebtnarea" align="right"><i title="Close" onclick="$('#open-modal').hide('slow');"
                class="fa fa-times-circle fl-r crs-pntr" style="font-size:27px;color:#6c757d;"></i></div>
        <br>
        <div class="container">
            <div class="">
                @foreach($popupAds as $popupAd)
                <div class="col-md-12 margin-bottom-15"
                    style="padding-right: 10px;padding-bottom: 10px;padding-left: 10px;">
                    <div class="adsContent">
                        @if($popupAd->adds_type == 'image')
                        <a class="adsLink" href="{{ 'http://'.$popupAd->image_link }}" target="_blank">
                            <img src="{{ asset('/uploads/adsImages/'.$popupAd->image) }}" class="adsImage"
                                style="height:300px;border-radius: 10px;">
                        </a>
                        @elseif($popupAd->adds_type == 'embed_code')
                        <?php echo $popupAd->embed_code ?>
                        @elseif($popupAd->adds_type == 'referral_code')
                        {{ $popupAd->referral_code }}
                        @else
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div style="position:fixed; right:0%; bottom:0%;z-index: 2;">
    <div id="messagepop"></div>
</div>

@endif
@php
$leftAds = \App\library\SiteHelper::getLeftAds($currentPage ,$adsUserId);
$rightAds = \App\library\SiteHelper::getRightAds($currentPage ,$adsUserId);
$addcls = 'col-lg-2 col-md-2';
$contCls =' col-lg-6 col-md-6';
if (isset($homeList)) {
if ($setting->view_style !== 'facebook') {
$addcls = 'col-lg-2 col-md-1';
$contCls =' col-lg-8 col-md-10';
}
} else {
$addcls = 'col-lg-2 col-md-2';
$contCls =' col-lg-8 col-md-8';
}

@endphp
    @if (!isset($homeList))
<?php 

$botAdsot = \App\library\SiteHelper::getBottomAds($currentPage ,$adsUserId)->toArray();
$topAdsot = \App\library\SiteHelper::getTopAds($currentPage ,$adsUserId)->toArray();
// $allAds = array_merge($rightAds->toArray(),$leftAds->toArray(), $topAdsot, $botAdsot);
$allAds = array_merge( $botAdsot);
$allAdsCount = count($allAds);
$addCnt = 0;
    ?>
    @if ($controller !== 'UserController')
    @php
    $leftAds = \App\library\SiteHelper::getLeftAds('otherpages',0);
    $rightAds = \App\library\SiteHelper::getRightAds('otherpages',0);
    $allAds = array_merge($rightAds->toArray(),$leftAds->toArray());
    $addCnt = 0;
    $allAdsCount = count($allAds);
    @endphp
    <?php while ($addCnt < $allAdsCount) { ?>
    <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
    <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
        <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
            {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
            <img class=""
                src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                alt="Card image cap">
            <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
        </a>
        <?php 
            $addCnt++; 
            ?>
    </div>
    <?php } } ?>
    @endif
    @endif
    <style>
        /* Left  and right add start *
        .cls-left-root, .cls-containt-rooot{
            float: left;
        }
        .main-cont.cont-home-page.root-home-facebook .cls-add-row {
            max-width: 114px;
        }

        .main-cont.cont-home-page.root-home-facebook .cls-add-row .cls-left-add-img img {
            max-width: 100%;
        }

        .main-cont.cont-home-page.root-home-facebook .container {
            max-width: 650px;
        }
        /* Left  and right add end*/

        .root-home-facebook .cls-containt-rooot.col-lg-6.col-md-6 {
            padding: 0px;
            margin: 0px;
        }

        .cls-footer {
            z-index: 99999;

        }

        .cls-footer .MS-content,
        .cls-footer .adsMargin {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }

        .cls-bottom-add-item img {
            max-height: 400px
        }

        .cls-bottom-add-item a span {
            opacity: 0.8;
            width: 100%;
            height: 25px;
            line-height: 25px;
            display: block;
            background-color: #000;
            text-align: center;
            position: relative;
            bottom: 25px;
        }

        ul.footer-social li {
            margin: 27px 5px 0 5px;
            padding: 0;
        }

        .social-section {
            width: 100%;
            height: 45px;
            text-align: center;
        }

        ul.footer-social li {
            height: 30px;
            font-size: 20px;
            display: inline-block;
            margin: 10px 11px 0 11px;
        }

        ul.footer-social li a {
            color: #FFF;
        }
    </style>
    <!-- Scroll top buttm -->
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <script>
        mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>
    <style>
        #myBtn {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Fixed/sticky position */
            bottom: 20px;
            /* Place the button at the bottom of the page */
            right: 30px;
            /* Place the button 30px from the right */
            z-index: 99;
            /* Make sure it does not overlap */
            border: none;
            /* Remove borders */
            outline: none;
            /* Remove outline */
            background-color: orange;
            /* Set a background color */
            color: white;
            /* Text color */
            cursor: pointer;
            /* Add a mouse pointer on hover */
            padding: 15px;
            /* Some padding */
            border-radius: 10px;
            /* Rounded corners */
            font-size: 18px;
            /* Increase font size */
        }

        #myBtn:hover {
            background-color: #c38108;
            /* Add a dark-grey background on hover */
        }
    </style>
    <div class="footer d-none  cls-footer">
        <div class="row  cls-social">
            <div class="col-md-12 social-section">
                <ul class="footer-social">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
        </div>
        @if(isset($controller) && in_array( $controller, ['HomeController', 'PublicPageController', 'NewDashboardController']))
        <div class=" adsMargin">
            <div class="">
                <div class="exampleSlider">
                    <div class="MS-content">
                        @php
                        $bottomAds = \App\library\SiteHelper::getBottomAds($currentPage,$adsUserId);
                        $countBottomAdds = (sizeof($bottomAds));
                        @endphp
                        @foreach($bottomAds as $bottomAd)
                        <div class="item">
                            <a href="{{ 'http://'.$bottomAd->image_link }}"><img height="50px"
                                    style="max-height:100px;height:100px"
                                    src="{{ asset('/uploads/adsImages/'.$bottomAd->image) }}"
                                    class="img-responsive"></a>
                            <span>{{$bottomAd->adds_name}} </span>

                        </div>
                        @endforeach


                        <div class="MS-controls">
                            <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                            <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        Date.prototype.addHours = function (h) {
            this.setTime(this.getTime() + (h * 60 * 60 * 1000));
            return this;
        };

        function startCoundownTimer(datetime, el) {
            // console.log(datetime,new Date());
            el = el ? el : null;
            var countDownDate = new Date(datetime.toLocaleString('en-US'));
            countDownDate = countDownDate.getTime();
            var x = setInterval(function () {

                // Get today's date and time
                var now = new Date(new Date().toLocaleString('en-US')).getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element
                if (el) {
                    var text = '';
                    if (days) {
                        text += days + " Days ";
                    }
                    if (hours) {
                        if (hours < 10) {
                            text += '0' + hours + ":";
                        } else {
                            text += hours + ":";
                        }
                    } else {
                        text += 00 + ":";
                    }
                    if (minutes) {
                        if (minutes < 10) {
                            text += '0' + minutes + ":";
                        } else {
                            text += minutes + ":";
                        }

                    } else {

                    }
                    if (seconds) {
                        if (seconds < 10) {
                            text += '0' + seconds;
                        } else {
                            text += seconds;
                        }
                    } else {
                        text += '00';
                    }

                    el.text(text);
                }

                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);
                    if (el) {
                        el.text("Expired");
                    }
                }
            }, 1000);
        }

        $(document).ready(function () {
            $('.countDownTimer').each(function (index, value) {
                var date = new Date($(this).attr('data-time'));
                var userTimezoneOffset = date.getTimezoneOffset() * 60000;
                var d = new Date(date.getTime() - userTimezoneOffset);
                // var newDate = d.addHours(data.hour);
                startCoundownTimer(d, $(this));
            })
        });
    </script>

    <script>
        $(document).ready(function () { 
            $('#listshow').click(function () { 
                $('#grid-display').hide();
                $('#list-display').show();
             });
             $('#gridshow').click(function () { 
                $('#list-display').hide();
                $('#grid-display').show();
             });
         });
    </script>

    <script>
        var idleTime = 0;
        $(document).ready(function () {
            // stiky();
            // stikyRight();
            //Increment the idle time counter every minute.
            var idleInterval = setInterval(timerIncrement, 1000); // 1 minute

            //Zero the idle timer on mouse movement.
            $(this).bind('mousewheel', function (e) {
                idleTime = 0;
                $('.cls-footer').addClass('d-none');
            });
            $(this).mousemove(function (e) {
                // idleTime = 0;
                // $('.cls-footer').addClass('d-none');
            });
            $(this).keypress(function (e) {
                $('.cls-footer').addClass('d-none');
                idleTime = 0;
            });
        });

        function timerIncrement() {
            idleTime = idleTime + 1;
            if (idleTime > 3) { // 
                $('.cls-footer').removeClass('d-none');
            }
        }
    </script>

    @stack('footerjs')
</body>
</html>