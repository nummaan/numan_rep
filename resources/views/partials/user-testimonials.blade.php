
<div class="container-fluid tmonial-main">
    <div class="row justify-content-center user-profile-row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="tmonila-content {{(request()->get('__s') || request()->get('page') > 0)?'hid_rat_sec':'shw_rat_sec'}}">
                <h5>Testimonials</h5>
                <ul class="tmonial-user-lst">
                    <li>
                        <a class="left-tmonial-btn" href="javascript:void(0);"><i class="fas fa-chevron-left"></i></a>
                        <a class="right-tmonial-btn" href="javascript:void(0);"><i class="fas fa-chevron-right"></i></a>
                    </li>
                    <li>
                        @php
                        $reviews_top = getUserTestimonials($user->id);
                        $count = 0;
                        $user_images = [];
                        @endphp
                        @foreach($reviews_top as $review)
                            @php
                                $count++;
                                $user_images[] = $review->avatar;
                            @endphp
                        <ul class="usr-rat-sec {{($count == 1)?'shw_rat_sec':'hid_rat_sec'}}" style="list-style: none;">
                            <li class="usr-rat-sec-cmnt">
                                &ldquo;
                                {{(!empty($review->review_text))?$review->review_text:'No comment'}}
                                &#8221;
                            </li>
                            <li class="usr-rat-sec-nam">
                                <h4>{{$review->name}}</h4>
                            </li>
                            <li class="usr-rat-sec-rev">
                                <input id="ownRating" name="ownRating"
                                       class="rating rating-loading own-rating"
                                       value="{{$review->review_number}}">
                            </li>
                        </ul>
                        @endforeach
                        <div class="tmonial-usr-images-sec">
                            @php $img_cnt = 0; @endphp
                            @foreach($user_images as $img)
                                @php $img_cnt++; @endphp
                            <img class="tmonial-user-image {{($img_cnt == 1)?'red_border':''}}" src="{{ asset('/uploads/avatars/' . $img) }}"
                                 id="avatar-img" style="display: {{($img_cnt > 3)?'none':'inline'}}"/>
                            @endforeach
                        </div>
                    </li>
                    @if($reviews_top->count() <= 0)
                        <li style="margin:10px 0 10px 0">
                            No Reviews Yet.
                        </li>
                    @endif
                    <li>
                        @if($user->id == auth()->user()->id)
                            <a class="btn btn-primary btn-danger view_all_tes_btn" href="{{url('userprofile?__s=true')}}">View All Testimonials</a>
                        @else
                            <a class="btn btn-primary btn-danger view_all_tes_btn" href="{{url("userprofile/$user->id?__s=true")}}">View All Testimonials</a>
                        @endauth
                        @if($user->id !== auth()->user()->id)
                        <a class="btn btn-primary btn-success add-testimonial-btn">Add Testimonials</a>
                         @endif
                    </li>
                </ul>

            </div>
            <div class="{{(request()->get('__s') || request()->get('page') > 0)?'shw_rat_sec':'hid_rat_sec'}}">
                <div>
                    <h5 style="text-align: center;padding-top: 10px;color:#fff">Testimonials Page</h5>
                </div>
                <ul class="users_tmonial_plist" style="list-style: none"
                    <li>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" >
                                @if($user->id !== auth()->user()->id)
                                    <a class="btn btn-primary btn-success add-testimonial-btn"  style="float: right;margin: 5px 5px 5px 10px">Add Testimonials</a>
                                @endif
                            </div>
                        </div>
                    </li>
                    @if(!empty($reviews))
                        @foreach($reviews as $review)
                            <li class="users_rating_list_plist">
                        <div class="row">
                            <div class="col-md-2 col-sm-3 col-xs-3">
                                <img class="tmonial-user-image usr-img-below" src="{{ asset('/uploads/avatars/' . $review->avatar) }}"
                                     id="avatar-img"/>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <div>
                                    <input id="ownRating" name="ownRating"
                                           class="rating rating-loading own-rating"
                                           value="{{$review->review_number}}">
                                </div>
                                <div>
                                    <h4>
                                       {{$review->name}}
                                    </h4>
                                </div>
                                <div>
                                    &ldquo;
                                    {{(!empty($review->review_text))?$review->review_text:'No comment'}}
                                    &#8221;
                                </div>
                            </div>
                        </div>
                    </li>
                        @endforeach
                     <li>
                         {{$reviews->links()}}
                     </li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</div>