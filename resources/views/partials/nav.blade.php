<?php
use Illuminate\Support\Facades\DB;
$site_info = DB::table('site_info')->get();
$info_element_array = array();
foreach ($site_info as $info_element) {
    $info_element_array[$info_element->attr_name] = $info_element->attr_value;
}
?>
<style>
    .fixed-top {
        opacity:0.9;
        z-index: 2222222222;
        top: 0px;
        position: fixed;
        width: 100%;
        background:#000;
    }
    .fixed-top .bg-light{
        background:#000 !important;
    }
    .fixed-side-bar{
        top: 80px;
        position: fixed;
    }
</style>
<script>

    $( document ).ready(function() {
        console.log( "document ready!" );
        $(window).scroll(function(){ // scroll event
            var windowTop = $(window).scrollTop(); // returns number
            if (165 < windowTop) {
                $('.cls-top-navbar').addClass('fixed-top');
            } else {
                $('.cls-top-navbar').removeClass('fixed-top');
            }
        });

    });
</script>
<nav class="navbar cls-top-navbar  navbar-expand-lg  navbar-expand-sm navbar-expand-xs  navbar-light bg-light" {{--style="background-image: url('/uploads/avatars/{{$info_element_array['header_right_pic']}}'); background-size: cover;"--}}>
    <div class="container-fluid">
    <span id="opensidebar" onclick="openNav()"><i class="fas fa-bars fa-2x mx-3"></i></span>
        <a class="navbar-brand" href="{{ url('/') }}">
            <img class="brand-logo" src="{{ URL::asset('uploads/avatars/'.$info_element_array['header_left_pic']) }}" alt="Brand Logo"
                 > {{$info_element_array['site_name']}}
        </a>
        <button class="navbar-toggler mob-menu-btn" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        {{-- Start desktop menu--}}
        <div class="collapse navbar-collapse desktop-menu" id="navbarSupportedContent">


            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto" style="float:right">
                @auth
                <li class="nav-item dropdown mt-4">
                    <div class="search-box-new">
                        <input type="text" placeholder="Search Users" class="search-in search-in-new" id="search" name="search"
                               autocomplete="off">
                        <a href="#" class="menu-search-icon-cont">
                        <i class="fas fa-search fa-2x"></i></a>

                        <table class="table table-bordered table-hover text-success user-search-table">
                            <tbody id="tbod">
                            </tbody>
                        </table>
                    </div>
                </li>
                @endauth
                <li class="nav-item dropdown mt-4">
                    @auth
                        @if (Auth::user()->avatar)
                            <a id="navbarDropdown" class="nav-link {{--dropdown-toggle--}}" href="{{url('/userprofile')}}" {{--role="button" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false" v-pre--}}
                            style="position:relative; padding-left:50px;padding-top: 13px;">
                                {{ Auth::user()->name }}
                                <a href="{{url('/userprofile')}}"> <img src="{{ asset('/uploads/avatars/' . Auth::user()->avatar) }}"
                                                                        style="width:32px; height:32px; position:absolute; top:7px; left:10px; border-radius:50%">
                                </a>
                                <span class="caret"></span>
                            </a>
                        @else
                            <a id="navbarDropdown" class="nav-link {{--dropdown-toggle--}}" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false" v-pre
                               style="position:relative; padding-left:50px;">
                                {{ Auth::user()->name }}
                                <img src="{{ asset('img/default.png') }}"
                                     style="width:32px; height:32px; position:absolute; top:0px; left:10px; border-radius:50%">
                                <span class="caret"></span>
                            </a>
                        @endif
                    @endauth
                <!--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">-->
                <!--
                        <a class="dropdown-item" href="{{ route('trainuser') }}">
                            Training
                        </a>
                        <a class="dropdown-item" href="{{ route('examuser') }}">
                            Exam
                        </a> -->
                <!-- <a class="dropdown-item" href="{{ route('settings') }}">
                            Settings
                        </a> -->

                <!-- <a class="dropdown-item" href="{{ route('faq.category.index') }}">
                            FAQ Category
                        </a> -->
                    <?php
                    /*if (Auth::user()) {
                    $login_user_id = Auth::user()->id;
                    $users_manus = DB::table('menu_options')
                        ->join('user_menu', 'user_menu.menu_options_id', '=', 'menu_options.id')
                        ->where('menu_options.id', "!=", 14)
                        ->where('menu_options.name', "!=", "Blog Admin")
                        ->where('menu_options.name', "!=", "Event Admin")
                        ->where('menu_options.name', "!=", "Category Setup")
                        ->where('menu_options.name', "!=", "More Admin")
                        ->where('menu_options.name', "!=", "Dispute Manager")
                        ->where('menu_options.name', "!=", "Bid Admin")
                        ->where('menu_options.name', "!=", "Training Setup")
                        ->where('menu_options.name', "!=", "Exam Setup")
                        ->where('menu_options.name', "!=", "Upcoming Services")
                        ->where('user_menu.user_id', $login_user_id)->get();
                        $help_desk_label = '';
                        $help_desk_link = '';
                         foreach ($users_manus as $users_manu){
                                if($users_manu->name == 'HelpDesk'){
                                    $help_desk_label = $users_manu->name;
                                    $help_desk_link = $users_manu->link;
                                }else{
                                    */?>
                <!--<a class="dropdown-item" href="{{--{{ url($users_manu->link) }}--}}"><?php //echo $users_manu->name; ?></a>-->
                    <?php
                    /*}
                }
            }*/
                    ?>

                <!-- <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a> -->

                    {{--
                </div>--}}
                </li>
                <!-- Authentication Links -->
            @guest
                <!--  <li class="nav-item">
                        <a class="nav-link" href="{{ route('blog') }}">
                            <i class="fab fa-blogger fa-2x" style="color:sandybrown"></i>
                        </a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">
                            <i class="far fa-user-circle fa-2x"></i>
                        </a>
                    </li>
                <!-- <li>
                        <div class="dropdown" style="margin-top:9px;!important">
                            <button class="btn btn-default dropdown-toggle look-like-btn" type="button" data-toggle="dropdown">More
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" style="text-decoration: none;">About Us</a></li>

                                <li>
                                    Helps
                                    <ul class="list-group">
                                        <li class="list-group-item">Faq</li>
                                        <li class="list-group-item">Dispute</li>
                                    </ul>
                                </li>
                                <li><a href="{{ URL('/upcoming_services') }}" style="text-decoration: none;">Upcoming Services</a></li>
                            </ul>
                        </div>
                    </li> -->
                    <li class="nav-item mt-2">
                        <a href="{{ route('aboutus')}}" style="text-decoration: none;font-size: 17px;">&nbsp;&nbsp;&nbsp;About Us</a></li>

                    </li>
            @else
                <!-- <li class="nav-item mt-4">
                                <a class="nav-link" href="{{ route('login') }}">
                                   <i class="fas fa-signal fa-1x"></i>
                                   Sort
                                </a>
                            </li> -->
                    <li class="nav-item mt-4">
                        <a class="nav-link" href="/home">

                            <i class="fas fa-home fa-1-5x"></i>
                        </a>

                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="/public-blog">

                            <i class="fab fa-blogger fa-1-5x" {{--style="color:sandybrown"--}}></i>
                        </a>

                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="/home?__t=1">

                            <i class="fas fa-business-time fa-1-5x"></i>
                        </a>

                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="/home?__t=2">

                            <i class="fas fa-calendar-alt fa-1-5x"></i>
                        </a>

                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="{{ route('chatdashboard') }}">
                            <i class="fas fa-comments fa-1-5x"></i>
                            {{--Chat--}}
                        </a>
                    </li>



                    <li class="nav-item mt-4">
                        <a class="nav-link" href="{{ URL('/saved_posts') }}">
                            <i class="fas fa-star fa-1-5x"></i>
                            {{--Saved--}}
                        </a>
                    </li>
                    <li class="nav-item mt-4">
                        <a class="nav-link newpost" href="#"  id="newpost"><i class="fas fa-plus-circle"></i> Post</a>
                    </li>


                    <li class="nav-item mt-4">
                        <div class="dropdown" >
                            <button class="btn btn-default {{--dropdown-toggle--}} look-like-btn nav-link" type="button" data-toggle="dropdown"><i class="fas fa-ellipsis-v fa-1-5x"></i>
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu drpdwn-scroll">
                                <li class="extras-item"><a class="dropdown-item" href="{{route('aboutus')}}" style="text-decoration: none;">About Us</a></li>
                                </li>
                                <li class="extras-item"><a class="dropdown-item" style="text-decoration: none;" href="{{ url('/privateChat/'.Auth::id().',55'.'/'.Auth::id().'/0') }}">Help Desk</a></li>
                                <li class="extras-item"><a class="dropdown-item" href="{{ route('faquser') }}" style="text-decoration: none;">FAQ</a></li>
                                <li class="extras-item"><a class="dropdown-item" href="/dispute" style="text-decoration: none;">Dispute</a></li>
                                {{--admin dropdown start--}}
                                @if (Auth::user())
                                    <li class="extras-item"><a class="dropdown-item" href="#" style="text-decoration: none;">Admin Menu</a></li>
                                    <li class="extras-item ml-4 dropdown-submenu">
                                        <ul>
                                            @php
                                                $login_user_id = Auth::user()->id;
                                                $users_manus = DB::table('menu_options')
                                                    ->join('user_menu', 'user_menu.menu_options_id', '=', 'menu_options.id')
                                                    ->where('menu_options.id', "!=", 14)
                                                    ->where('menu_options.name', "!=", "Blog Admin")
                                                    ->where('menu_options.name', "!=", "Event Admin")
                                                    ->where('menu_options.name', "!=", "Category Setup")
                                                    ->where('menu_options.name', "!=", "More Admin")
                                                    ->where('menu_options.name', "!=", "Dispute Manager")
                                                    ->where('menu_options.name', "!=", "Bid Admin")
                                                    ->where('menu_options.name', "!=", "Training Setup")
                                                    ->where('menu_options.name', "!=", "Exam Setup")
                                                    ->where('menu_options.name', "!=", "Upcoming Services")
                                                    ->where('menu_options.name', "!=", "HelpDesk Agent")
                                                    ->where('user_menu.user_id', $login_user_id)->get();
                                                $help_desk_label = '';
                                                $help_desk_link = '';
                                            @endphp
                                            @foreach ($users_manus as $users_manu)
                                                @if($users_manu->name == 'HelpDesk')
                                                    @php
                                                        $help_desk_label = $users_manu->name;
                                                        $help_desk_link = $users_manu->link;
                                                    @endphp
                                                @else
                                                    <li class="extras-item">
                                                        <a class="dropdown-item" href="{{ url($users_manu->link) }}"><?php echo $users_manu->name; ?></a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                                {{--admin dropdown end--}}
                                <li class="extras-item"><a class="dropdown-item" href="{{ URL('/upcoming_services') }}" style="text-decoration: none;">Upcoming Services</a></li>
                                <li class="extras-item ml-4">
                                    <a class="dropdown-item" href="{{ route('login') }}"
                                       data-toggle="modal" data-target="#filter-modal">
                                        <i class="fas fa-sliders-h fa-1x"></i>
                                        Filters
                                    </a>
                                    <a class="dropdown-item" href=" {{ route('demopage')}}">
                                        Demo Page
                                    </a>
                                    <a class="dropdown-item" href=" {{ route('home2')}}">
                                        Home 2
                                    </a>
                                    <a class="dropdown-item" href="{{ route('newdashboard') }}">newdashboard</a>
                                </li>
                                <li class="extras-item ml-4 dropdown-submenu">
                                    <!-- <i class="fas fa-plus-circle fa-1x"></i> Post</span> -->
                                    <ul>
                                    <!-- <li class="extras-item"><a class="dropdown-item" href="{{ route('buyer.create') }}">
                                            Want to buy
                                        </a></li>
                                        <li class="extras-item"><a class="dropdown-item" href="{{ route('seller.create') }}">
                                            Want to Sell
                                        </a></li>
                                        <li class="extras-item"><a class="dropdown-item" href="{{ route('article.create') }}">
                                            Article Post
                                        </a></li>
                                        <li class="extras-item"><a class="dropdown-item" href="{{ route('AdvertisementPage') }}">
                                            Advertisement
                                        </a></li> -->
                                        <li class="extras-item"><a class="dropdown-item" href="{{ route('coupons') }}">
                                                Coupon
                                            </a></li>
                                        <li class="extras-item"><a class="dropdown-item" href="{{ url('CategorySetup') }}">
                                                Category Setup
                                            </a></li>

                                        <li class="extras-item">
                                            <a class="dropdown-item" href="{{ route('trainuser') }}">
                                                Training
                                            </a></li>
                                        <li class="extras-item">
                                            <a class="dropdown-item" href="{{ url('/trainsetup') }}">
                                                Training Setup
                                            </a></li>
                                        <li class="extras-item">
                                            <a class="dropdown-item" href="{{ route('examuser') }}">
                                                Exam
                                            </a></li>
                                        <li class="extras-item">
                                            <a class="dropdown-item" href="{{ url('/examsetup') }}">
                                                Exam Setup
                                            </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </li>
                    <li class="nav-item mt-4">
                        <a class="dropdown-item nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt fa-1-5x"></i>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>
                    </li>


                @endguest
            </ul>
        </div>
        {{--End desktop menu--}}

        {{--Start mobile menu--}}
        <div class="mobile-menu">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto navbar-mobile" style="float:right">
                @auth
                <li class="nav-item dropdown mt-4">
                    <div class="search-box-new">
                        <input type="text" placeholder="Search" class="search-in search-in-new" id="search" name="search"
                               autocomplete="off">
                        <a href="#" class="menu-search-icon-cont">
                            <i class="fas fa-search fa-2x"></i></a>

                        <table class="table table-bordered table-hover text-success user-search-table">
                            <tbody id="tbod" class="search-usr-new-body">
                            </tbody>
                        </table>
                    </div>
                </li>
                @endauth
                <li class="nav-item dropdown mt-4">
                    @auth
                        @if (Auth::user()->avatar)
                            <a id="navbarDropdown" class="nav-link {{--dropdown-toggle--}}" href="{{url('/userprofile')}}" {{--role="button" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false" v-pre--}}
                            style="position:relative; padding-left:50px;">
                                {{ Auth::user()->name }}
                                <a href="{{url('/userprofile')}}"> <img src="{{ asset('/uploads/avatars/' . Auth::user()->avatar) }}"
                                                                        style="width:32px; height:32px; position:absolute; top:0px; left:10px; border-radius:50%">
                                </a>
                                <span class="caret"></span>
                            </a>
                        @else
                            <a id="navbarDropdown" class="nav-link {{--dropdown-toggle--}}" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false" v-pre
                               style="position:relative; padding-left:50px;">
                                {{ Auth::user()->name }}
                                <img src="{{ asset('img/default.png') }}"
                                     style="width:32px; height:32px; position:absolute; top:0px; left:10px; border-radius:50%">
                                <span class="caret"></span>
                            </a>
                        @endif
                    @endauth
                </li>
                <!-- Authentication Links -->
            @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">
                            <i class="far fa-user-circle fa-2x"></i>
                        </a>
                    </li>
                    <li class="nav-item mt-2">
                        <a href="{{ route('aboutus')}}" style="text-decoration: none;font-size: 17px;">&nbsp;&nbsp;&nbsp;About Us</a></li>

                    </li>
                @else
                    <li class="nav-item mt-4">
                        <a class="nav-link newpost" href="#"  id="newpost"><i class="fas fa-plus-circle"></i> Post</a>
                    </li>
                    <li class="nav-item mt-4" id="mob_menu_list_btn">
                        <a class="nav-link" href="#"><i class="fas fa-ellipsis-v fa-2x"></i></a>
                    </li>
                @endguest
            </ul>
            <ul class="mob-menu-list" style="display: none">
                <li>
                <ul class="mob-menu-list-upper">
                    <li class="nav-item mt-4">
                        <a class="nav-link" href="/public-blog">

                            <i class="fab fa-blogger fa-1-5x" {{--style="color:sandybrown"--}}></i>
                        </a>

                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="/home?__t=1">

                            <i class="fas fa-business-time fa-1-5x"></i>
                        </a>

                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="/home?__t=2">

                            <i class="fas fa-calendar-alt fa-1-5x"></i>
                        </a>

                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="{{ route('chatdashboard') }}">
                            <i class="fas fa-comments fa-1-5x"></i>
                            {{--Chat--}}
                        </a>
                    </li>

                    <li class="nav-item mt-4">
                        <a class="nav-link" href="{{ URL('/saved_posts') }}">
                            <i class="fas fa-star fa-1-5x"></i>
                            {{--Saved--}}
                        </a>
                    </li>

                    <li class="nav-item mt-4">
                        <a class="dropdown-item nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt fa-1-5x"></i>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>
                    </li>
                </ul>
                </li>
                <li class="mob-center-divider"></li>
                <li>
                    <ul class="">
                        <li class="extras-item"><a class="dropdown-item" href="{{route('aboutus')}}" style="text-decoration: none;">About Us</a></li>
                        </li>
                        <li class="extras-item"><a class="dropdown-item" style="text-decoration: none;" href="{{ url('/privateChat/'.Auth::id().',55'.'/'.Auth::id().'/0') }}">Help Desk</a></li>
                        <li class="extras-item"><a class="dropdown-item" href="{{ route('faquser') }}" style="text-decoration: none;">FAQ</a></li>
                        <li class="extras-item"><a class="dropdown-item" href="/dispute" style="text-decoration: none;">Dispute</a></li>
                        {{--admin dropdown start--}}
                        @if (Auth::user())
                            <li class="extras-item"><a class="dropdown-item" href="#" style="text-decoration: none;">Admin Menu</a></li>
                            <li class="extras-item ml-4 dropdown-submenu">
                                <ul>
                                    @php
                                        $login_user_id = Auth::user()->id;
                                        $users_manus = DB::table('menu_options')
                                            ->join('user_menu', 'user_menu.menu_options_id', '=', 'menu_options.id')
                                            ->where('menu_options.id', "!=", 14)
                                            ->where('menu_options.name', "!=", "Blog Admin")
                                            ->where('menu_options.name', "!=", "Event Admin")
                                            ->where('menu_options.name', "!=", "Category Setup")
                                            ->where('menu_options.name', "!=", "More Admin")
                                            ->where('menu_options.name', "!=", "Dispute Manager")
                                            ->where('menu_options.name', "!=", "Bid Admin")
                                            ->where('menu_options.name', "!=", "Training Setup")
                                            ->where('menu_options.name', "!=", "Exam Setup")
                                            ->where('menu_options.name', "!=", "Upcoming Services")
                                            ->where('menu_options.name', "!=", "HelpDesk Agent")
                                            ->where('user_menu.user_id', $login_user_id)->get();
                                        $help_desk_label = '';
                                        $help_desk_link = '';
                                    @endphp
                                    @foreach ($users_manus as $users_manu)
                                        @if($users_manu->name == 'HelpDesk')
                                            @php
                                                $help_desk_label = $users_manu->name;
                                                $help_desk_link = $users_manu->link;
                                            @endphp
                                        @else
                                            <li class="extras-item">
                                                <a class="dropdown-item" href="{{ url($users_manu->link) }}"><?php echo $users_manu->name; ?></a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                        {{--admin dropdown end--}}
                        <li class="extras-item"><a class="dropdown-item" href="{{ URL('/upcoming_services') }}" style="text-decoration: none;">Upcoming Services</a></li>
                        <li class="extras-item ml-4">
                            <a class="dropdown-item" href="{{ route('login') }}"
                               data-toggle="modal" data-target="#filter-modal">
                                <i class="fas fa-sliders-h fa-1x"></i>
                                Filters
                            </a>
                            <a class="dropdown-item" href=" {{ route('demopage')}}">
                                Demo Page
                            </a>
                            <a class="dropdown-item" href=" {{ route('home2')}}">
                                Home 2
                            </a>
                        </li>
                        <li class="extras-item ml-4 dropdown-submenu">
                            <ul>
                                <li class="extras-item"><a class="dropdown-item" href="{{ route('coupons') }}">
                                        Coupon
                                    </a></li>
                                <li class="extras-item"><a class="dropdown-item" href="{{ url('CategorySetup') }}">
                                        Category Setup
                                    </a></li>

                                <li class="extras-item">
                                    <a class="dropdown-item" href="{{ route('trainuser') }}">
                                        Training
                                    </a></li>
                                <li class="extras-item">
                                    <a class="dropdown-item" href="{{ url('/trainsetup') }}">
                                        Training Setup
                                    </a></li>
                                <li class="extras-item">
                                    <a class="dropdown-item" href="{{ route('examuser') }}">
                                        Exam
                                    </a></li>
                                <li class="extras-item">
                                    <a class="dropdown-item" href="{{ url('/examsetup') }}">
                                        Exam Setup
                                    </a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    {{--End mobile menu--}}


    <!-- <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">More
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li>About Us</li>
                <li>Help Us</li>
                <li>Upcoming Services</li>
            </ul>
        </div> -->
    </div>
</nav>

