
    <div class="topleftchatdiv" >
       <input type="text" placeholder="Search with name" class="form-control search-box" id="search" name="search" autocomplete="off">
       <a class="btn dropdown-toggle alink" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="true">
       All conversation
       </a>
       <div class="dropdown-menu">
          <ul>
             <li class="dropdown-item "  id="unrea" aria-expanded="true" >Unread</li>
             <li class="dropdown-item" href="#">Archive</li>
             <li  class="dropdown-item "  aria-expanded="true" id="sss">Spam</li>
             <li class="dropdown-item" id="report" aria-expanded="true">Report</li>
          </ul>
          <div id="lll">
          </div>
       </div>
       {{-- spam body --}}
       <div id="spambody" >
       </div >
       {{-- unread body --}}
       <div id="unread" >
       </div>
       {{--report body --}}
       <div id="reportbody" >
       </div>
       {{-- level --}}
       {{--
       <div id="indeviduallevelsearch">
       </div>
       --}}
       <table class="table table-bordered table-hover text-success" >
          <tbody id="tbod">
          </tbody>
       </table>
    </div>
    <div class="chat-members">
       <ul class="table" >
          @foreach($receivers as $receiver)
          @php
          $userid = Auth::user()->id;
          $receiverid = $receiver->id;
          if($userid==$receiverid){
          continue;
          }
          if($receiverid > $userid){
          $chatRoomId = $userid.','.$receiver->id;
          }
          else{
          $chatRoomId = $receiver->id.','.$userid;
          }
          $romid=App\Chatroom::where('chatRoomId',$chatRoomId)->first();
          if(empty($romid)){
          $chat = new Chatroom;
          $chat->chatRoomId = $chatRoomId;
          $chat->save();
          $romid = Chatroom::where('id', $chat->id)->first();
          }
          $romid=$romid->id;
          $messagecont=App\Message::where('RoomId',$romid)
          ->where('readWriteStatus','!=',1)
          ->where('sender','!=',$userid)
          ->count();
          $message='';
          if($messagecont>0){
          $messageunread = App\Message::where('RoomId',$romid)
          ->where('readWriteStatus','!=',1)
          ->where('sender','!=',$userid)
          ->orderBy('created_at','DESC')
          ->first();
          }
          else{
          $message = App\Message::where('RoomId',$romid)->orderBy('created_at','DSEC')->first();
          $messageunread=0;
          }
          $timestring=' ';
          //dd($message);
          if($message!=''){
          $time = strtotime($message->created_at);
          $date= date('Y-m-d H:i:s', $time);
          $date = date_create($date);
          $nowdate = date("Y-m-d H:i:s");
          $nowdate = date_create($nowdate);
          $diff = date_diff($nowdate, $date);
          if($diff->s > 0 && $diff->i <1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0){
          $timestring='just now';
          }elseif ($diff->i >=1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
          $timestring=$diff->i.'m  ago';
          }elseif ($diff->i >=1  && $diff->h >= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
          $timestring=$diff->h.'h    '.$diff->i.'m ago';
          }elseif($diff->h >= 0 && $diff->d >=0 && $diff->m <=0 && $diff->y <=0){
          $timestring=$diff->d.'d  '.$diff->h.'h ago';
          }elseif($diff->d >=0 && $diff->m >=0 && $diff->y <=0){
          $timestring=$diff->m.'months  '.$diff->d.'days ago';
          }
          elseif ($diff->m >=0 && $diff->y >=0) {
          $timestring=$diff->y.' year '.$diff->m.' months ago';
          }
          }
          if($messageunread){
          $time = strtotime($messageunread->created_at);
          $date= date('Y-m-d H:i:s', $time);
          $date = date_create($date);
          $nowdate = date("Y-m-d H:i:s");
          $nowdate = date_create($nowdate);
          $diff = date_diff($nowdate, $date);
          if($diff->s > 0 && $diff->i <1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0){
          $timestring='just now';
          }elseif ($diff->i >=1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
          $timestring=$diff->i.'m  ago';
          }elseif ($diff->i >=1  && $diff->h >= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
          $timestring=$diff->h.'h  '.$diff->i.'m ago';
          }elseif($diff->h >= 0 && $diff->d >=0 && $diff->m <=0 && $diff->y <=0){
          $timestring=$diff->d.' days  '.$diff->h.'h ago';
          }elseif($diff->d >=0 && $diff->m >=0 && $diff->y <=0){
          $timestring=$diff->m.' months '.$diff->d.' days ago';
          }
          elseif ($diff->m >=0 && $diff->y >=0) {
          $timestring=$diff->y.' year '.$diff->m.' months ago';
          }
          }
          $starlevel=App\Level::where('userleveler',$userid)
          ->where('userbeenleveled',$receiver->id)
          ->where('value','Star')->count();
          //echo $timestring;
          @endphp
          @if($messageunread)
          <li class="chatliststyle"  style="overflow: auto;background:lightgray;">
             <img class="receiver-profile-image float-left" style="display:inline" src="{{asset('/uploads/avatars/'.$receiver->avatar)}}" height="50px" width="50px"  alt="{{asset('uploads/avatars/defaultpic.jpg')}}"  >
             @if($receiver->onlineStatus==1)
             <sub class="badge badge-success" style="position: relative; right: 0%; top: 35px; color: rgb(255, 255, 255);display:inline;float:left">.</sub>
             @else
             <sub class="badge badge-warning" style="position: relative; right: 0%; top: 35px; color: rgb(255, 255, 255);display:inline;float:left">.</sub>
             @endif
             <a  href="{{url('privateChat/'.$chatRoomId.'/'.$userid.'/'.$helpDeskUser)}}">
                <div class="chatlistname" style="float:left">
                   <h3 class="alink"  style="display:inline" >  {{$receiver->name}}  </h3>
                   @if($starlevel !=0)
                   <i class="far fa-star time staryellow"></i><span class="time">{{$timestring}}</span>
                   @else
                   <i class="far fa-star time ">{{$timestring}}</i>
                   @endif
                </div>
                <br>
                <h5 class="col-md-10 well-sm message_font" style="display:inline;" ><b>{{ $messageunread->message}}</b></h5>
                <span class="badge badge-success">{{$messagecont}}</span>
             </a>
          </li>
          @elseif($message)
          <li class="chatliststyle" style="overflow: auto;">
             <img class="receiver-profile-image float-left" style="display:inline" src="{{asset('/uploads/avatars/'.$receiver->avatar)}}" height="50px" width="50px" alt="{{asset('uploads/avatars/defaultpic.jpg')}}"  >
             @if($receiver->onlineStatus==1)
             <sub class="badge badge-success" style="position: relative; right: 0%; top: 35px; color: rgb(255, 255, 255);display:inline;float:left">.</sub>
             @else
             <sub class="badge badge-warning" style="position: relative; right: 0%; top: 35px; color: rgb(255, 255, 255);display:inline;float:left">.</sub>
             @endif
             <a  href="{{url('privateChat/'.$chatRoomId.'/'.$userid.'/'.$helpDeskUser)}}">
                <div class="chatlistname" style="float:left">
                   <h3 class="alink" style="display:inline" >   {{$receiver->name}}</h3>
                   @if($starlevel !=0)
                   <i class="far fa-star time staryellow"></i><span class="time">{{$timestring}}</span>
                   @else
                   <i class="far fa-star time ">{{$timestring}}</i>
                   @endif
                </div>
                <br>
                <h5 class="message_font col-md-10 " style= "display:inline;text-align: left;overflow:auto;" >{{ $message->message}}</h5>
             </a>
          </li>
          @endif
          @endforeach
       </ul>
    </div>
