<div id="right-sidebar" class="col-2">
    @php
        $advertisements = \App\Advertisement::where('position','right')->take(5)->get();
    @endphp
    @foreach ($advertisements as $advertisement)
    <div>
        <img src="{{ asset('uploads/adsImages/'.$advertisement->image) }}" alt="{{ $advertisement->adds_name }}" class="w-100 h-auto">
    </div>
    @endforeach
</div>