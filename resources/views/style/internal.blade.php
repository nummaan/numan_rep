<style>
    html {
        margin: 0px !important;
    }

    .main-cont {
        /* margin: 50px; */
    }

    .cls-left-add-title,
    .cls-right-add-title {
        transform: translate(0px, 0px);
    }

    .cls-left-add-title span,
    .exampleSlider .MS-content span {
        height: 22px;
        background-color: #000;
        width: 100%;
        display: block;
        line-height: 22px;
        text-align: center;
        bottom: 0px;
        position: absolute;
        opacity: 0.8;
    }

    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;

        color: white;
        text-align: center;
    }

    .cls-social {
        background-color: orange;
    }

    .adsMargin {
        background-color: #fff;
    }

    @media (max-width: 700px) {
        .foot-add-row {
            display: none;
        }
    }

    .heart {
        cursor: pointer;
        height: 50px;
        width: 50px;
        background-image: url('https://abs.twimg.com/a/1446542199/img/t1/web_heart_animation.png');
        background-position: left;
        background-repeat: no-repeat;
        background-size: 2900%;
    }

    .heart:hover {
        background-position: right;
    }

    .is_animating {
        animation: heart-burst .8s steps(28) 1;
    }

    @keyframes heart-burst {
        from {
            background-position: left;
        }

        to {
            background-position: right;
        }
    }


    .text-white {}

    .look-like-btn {
        background-color: Transparent;
        background-repeat: no-repeat;
        border: none !important;
        cursor: pointer !important;
        overflow: hidden;
        color: #364759;
        box-shadow: none !important;
    }

    .cls-right-add-img img {
        width: 100%;
        min-height: 100px;
    }

    .cls-right-add-block,
    .cls-left-add-block {
        margin-bottom: 10px;
    }

    .cls-right-add-block div {
        padding: 0px;
        padding-left: 2px;
    }

    .cls-right-add-img span {
        position: absolute;
        transform: translate(-100%, 0px);
        background: #000;
        color: #fff;
        opacity: 0.6;
        width: 100%;
        bottom: 10px;
        height: 30px;
        text-align: center;
    }


    .sm-add-root {
        margin-bottom: 10px;
        max-height: 200px;
    }

    .sm-add-root img {
        width: 100%;
        min-height: 100px;
        max-height: 200px;
    }

    .sm-add-root div {
        padding: 0px;
        padding-left: 2px;
    }

    .sm-add-root span {
        position: absolute;
        transform: translate(-100%, 170px);
        background: #000;
        color: #fff;
        opacity: 0.6;
        width: 93%;
        height: 30px;
        text-align: center;
    }

    .cls-left-add-root.d-none.d-md-block {
        padding-left: 14px;
    }

    .foot-add-row .col-md-1 span {
        position: absolute;
        background: black;
        transform: translate(-100%, 0%);
        width: 101%;
        bottom: 0;
        height: 38px;
        opacity: 0.5;
    }

    .foot-add-row .col-md-1 {
        margin: 0px;
        padding: 0px;
    }

    .foot-add-row .col-md-1 img {
        width: 100%;
        height: 100px;
    }

    .overlay_badge_buy.buyDelete,
    .overlay_badge_buy.eventDelete,
    .overlay_badge_buy.buyEdit,
    .overlay_badge_buy.eventEdit,
    .overlay_badge_buy.savedButton {
        max-width: 30px;
        float: right;
        left: unset !important;
        right: 0px;
    }

    .overlay_badge_buy.buyDelete,
    .overlay_badge_buy.eventDelete {
        right: 65px
    }

    .overlay_badge_buy.buyEdit,
    .overlay_badge_buy.eventEdit {
        right: 32px
    }

    .cls-add-row {
        /* min-height:<?php echo $setting->view_style == 'facebook' ? 400 : 450 ?>px; */
    }

    .cls-add-row img,
    .cls-add-row div {
        max-width: 100%;
        min-width: 100px;
        width: 100%;
    }

    .root-home-pinterest .col-lg-1.col-md-1 .cls-right-add-img span {
        transform: translate(0px, 10px);
    }

    @media(min-width: 1200px) {

        .cls-cnt {
            min-width: <?php echo $setting->view_style=='facebook'? 50: 80 ?>% !important;
            max-width: <?php echo $setting->view_style=='facebook'? 50: 80 ?>% !important;
        }

        .cls-left-add-root,
        .cls-right-add-root {
            min-width: <?php echo $setting->view_style=='facebook'? 25: 9 ?>% !important;
            padding: 20px;
        }

    }

    /* Left Right Add Start */

    .cls-left-add-root,
    .cls-right-add-root {
        float: left;
        max-width: 100px;
    }

    /* Left Right Add end */
</style>
<style type="text/css">
    /*.hoverSet :hover {
             border: 5px solid ;
             overflow: hidden;
         }*/
    .hoverSet:hover .set {

        border: 4px solid orange;
        padding: 2px;

    }

    #savedButton {
        display: none;
    }

    #eventDelete {
        display: none;
    }

    #buyDelete {
        display: none;
    }

    #eventEdit {
        display: none;
    }

    #buyEdit {
        display: none;
    }

    .hoverSet:hover #savedButton {
        display: block;
    }

    .hoverSet:hover #eventDelete {
        display: block;
    }

    .hoverSet:hover #buyDelete {
        display: block;
    }

    .hoverSet:hover #eventEdit {
        display: block;
    }

    .hoverSet:hover #buyEdit {
        display: block;
    }

    .overlay_badge_buy:hover .editButton {
        border: 1px solid orange;
        padding: 2px;
    }

    .overlay_badge_buy:hover .deleteButton {
        border: 1px solid orange;
        padding: 2px;
    }

    .overlay_badge_buy:hover .savedButton {
        border: 1px solid orange;
        padding: 2px;
    }
</style>
<style type="text/css">
    .overlay_badge_sell {
        position: absolute;
        top: 0;
        left: 0;
        background-color: red;
        color: white;
        padding-left: 10px;
        padding-right: 10px;
    }

    .overlay_badge_buy {
        position: absolute;
        float: right;
        top: 0;
        left: 0;
        background-color: green;
        color: white;
        pointer-events: auto;
        padding-left: 10px;
        padding-right: 10px;
    }

    .overlay_badge_blog {
        position: absolute;
        float: right;
        top: 0;
        left: 0;
        background-color: #F4A460;
        color: white;
        pointer-events: auto;
        padding-left: 10px;
        padding-right: 10px;
    }

    .overlay_badge_event {
        position: absolute;
        float: right;
        top: 0;
        left: 0;
        background-color: purple;
        color: white;
        pointer-events: auto;
        padding-left: 10px;
        padding-right: 10px;
    }

    .divback {
        background-color: #e3e3e3;
        padding: 5px;
        margin: 5px;
    }

    .bigtext {
        color: black;
        font-weight: bolder;
    }

    .ordertext {
        color: #a658a6;
    }

    .bidtext {
        color: #4e66c4;
        align-self: flex-end;
    }

    .typing {
        background-color: #f47e2b;
        border-color: #f47e2b;
    }

    .typing i {
        display: block !important;
    }

    .cls-padding-2 {
        padding: 3px;
    }

    #timeText {
        color: black !important;
    }


    .cls-left-add-block {
        margin-bottom: 10px;
    }

    .cls-left-add-block div {
        padding: 0px;
        padding-left: 2px;
    }

    .footer {
        position: fixed;
    }

    .MYcontainer {

        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    .font-family-poppins {
        font-family: 'Poppins', sans-serif !important;
    }
</style>
<style>
    ul {
        list-style-type: none;
        -webkit-padding-start: 1rem;
        padding-inline-start: 1rem;
    }

    #mySidenav .nav-item:hover {
        border: orangered solid 3px;
        box-sizing: border-box;
    }

    .nav-item:active {
        border: orangered solid 3px;
        box-sizing: border-box;
    }

    /* @media only screen and (max-width: 768px) {
        #sidebar{
            width:100% !important;
        } */
    }
</style>
<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    .takephoto {
        background-color: #fff;
        border-color: #000;

        border-width: 1px solid;

        padding: 10px;

        width: 100%;

        text-align: center;

        border-radius: 5px;

    }



    .continue {

        background-color: #006dbc;
        border-color: #006dbc;

        box-shadow: gray;

        padding: 10px;

        width: 100%;

        text-align: center;

        border-radius: 5px;

        color: #fff;
    }

    .cancel {
        background-color: #f47d2b;
        border-color: #f47d2b;
        box-shadow: gray;
        padding: 10 px;
        width: 100 %;
        text-align: center;
        border-radius: 5px;
        color: #fff;
    }

    .skip {
        background-color: #ef4726;
        border-color: #ef4726;
        box-shadow: gray;
        padding: 10px;
        width: 100%;
        text-align: center;
        border-radius: 5px;
        color: #fff;
    }

    .button-blue {
        box-shadow: gray;
        padding: 20px;
        width: 100%;
        text-align: left;
        border-radius: 3px;
        color: #000;

    }



    .button-blue:hover {

        background-color: #006dbc;
        border-color: #006dbc;

    }



    .button-blue:focus {

        background-color: #006dbc;
        border-color: #006dbc;

    }



    /* The Modal (background) */

    .modal {

        display: none;

        /* Hidden by default */

        position: fixed;

        /* Stay in place */

        z-index: 1;

        /* Sit on top */

        padding-top: 100px;

        /* Location of the box */

        left: 0;

        top: 0;

        width: 100%;

        /* Full width */

        height: 100%;

        /* Full height */

        overflow: auto;

        /* Enable scroll if needed */

        background-color: rgb(0, 0, 0);

        /* Fallback color */

        background-color: rgba(0, 0, 0, 0.4);

        /* Black w/ opacity */

    }



    /* Modal Content */

    .modal-content {

        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;

    }



    /* The Close Button */

    .close {

        color: #aaaaaa;
        float: right !important;
        font-size: 28px;
        font-weight: bold;
    }

    .close: hover,
    .close: focus {
        color: #000;

        text-decoration: none;

        cursor: pointer;

    }
</style>